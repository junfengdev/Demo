//
//  NSObject+UIKit.h
//  Demo
//
//  Created by lijunfeng on 2016/12/4.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (UIKit)

#pragma mark - UILabel


/**
 文字和大小

 @param text 文字
 @param size 大小
 */
+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)size;

/**
 字体颜色和大小

 @param textColor 颜色
 @param size 大小
 */
+ (UILabel *)labelTextColor:(UIColor *)textColor fontSize:(CGFloat)size;


/**
 文字、颜色、大小、对齐方式、行数、背景颜色、根据宽度调整字体大小

 @param text 文字
 @param color 颜色
 @param size 大小
 @param textAlignment 对齐方式
 @param numberOfLines 行数
 @param backgroundColor 背景颜色
 @param adjustsFontSizeToFitWidth 根据宽度调整字体大小 默认yes
 */
+ (UILabel *)labelWithText:(NSString *)text
                 textColor:(UIColor *)color
                  fontSize:(CGFloat)size
             textAlignment:(NSTextAlignment)textAlignment
             numberOfLines:(NSInteger)numberOfLines
           backgroundColor:(UIColor *)backgroundColor
 adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth;



@end
