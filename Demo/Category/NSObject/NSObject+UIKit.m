//
//  NSObject+UIKit.m
//  Demo
//
//  Created by lijunfeng on 2016/12/4.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "NSObject+UIKit.h"

@implementation NSObject (UIKit)


+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)size
{
    return [self labelWithText:text textColor:[UIColor whiteColor] fontSize:size textAlignment:NSTextAlignmentLeft numberOfLines:1 backgroundColor:nil adjustsFontSizeToFitWidth:YES];
}

+ (UILabel *)labelTextColor:(UIColor *)textColor fontSize:(CGFloat)size
{
    return [self labelWithText:nil textColor:textColor fontSize:size textAlignment:NSTextAlignmentLeft numberOfLines:1 backgroundColor:nil adjustsFontSizeToFitWidth:YES];
}


+ (UILabel *)labelWithText:(NSString *)text
                 textColor:(UIColor *)color
                  fontSize:(CGFloat)size
             textAlignment:(NSTextAlignment)textAlignment
             numberOfLines:(NSInteger)numberOfLines
           backgroundColor:(UIColor *)backgroundColor
 adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth
{
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.font = [UIFont systemFontOfSize:size];
    label.textAlignment = textAlignment;
    label.numberOfLines = numberOfLines;
    label.backgroundColor = backgroundColor;
    label.adjustsFontSizeToFitWidth = adjustsFontSizeToFitWidth;
    return label;
}

@end
