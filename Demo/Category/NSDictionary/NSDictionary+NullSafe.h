//
//  NSDictionary+NullSafe.h
//  Demo
//
//  Created by lijunfeng on 2016/10/23.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NullSafe)

+ (id)dictionaryWithFilterNullObject:(id)object;

@end

/*
 处理网络请求回来的数据
 * 将字典中的null值全部替换为空字符串
*/
