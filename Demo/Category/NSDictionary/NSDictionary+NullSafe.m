//
//  NSDictionary+NullSafe.m
//  Demo
//
//  Created by lijunfeng on 2016/10/23.
//  Copyright © 2016年 hm. All rights reserved.
//

//将NSDictionary中的Null类型的项目转化成@""

#import "NSDictionary+NullSafe.h"

@implementation NSDictionary (NullSafe)

#pragma mark - public //类型识别:将所有的NSNull类型转化成@""
+ (id)dictionaryWithFilterNullObject:(id)object{
  
    if ([object isKindOfClass:[NSNull class]]){
        return [self nullString];
    }
    else if ([object isKindOfClass:[NSDictionary class]]) {
        return [self nullDictionary:object];
    }
    else if ([object isKindOfClass:[NSArray class]]){
        return [self nullArray:object];
    }
    else if ([object isKindOfClass:[NSString class]]){
        return [self stringToString:object];
    }
    else if ([object isKindOfClass:[NSNumber class]]){
        return [self nullNumber:object];
    }
    else{
        return object;
    }
}

#pragma mark - private NSArNSDictionaryray
+ (NSDictionary *)nullDictionary:(NSDictionary *)dic{
    NSArray *allKeys = [dic allKeys];
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < allKeys.count; i ++) {
        id obj = [dic objectForKey:allKeys[i]];
        obj = [self dictionaryWithFilterNullObject:obj];
        [tempDic setObject:obj forKey:allKeys[i]];
    }
    return tempDic;
}

#pragma mark - private NSArray
+ (NSArray *)nullArray:(NSArray *)arr{
    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < arr.count; i ++) {
        id obj = arr[i];
        obj = [self dictionaryWithFilterNullObject:obj];
        [tempArr addObject:obj];
    }
    return tempArr;
}

#pragma mark - private NSNumber
+ (NSString *)nullNumber:(NSNumber *)number{
    NSString *numStr = [[NSNumberFormatter alloc] stringFromNumber:number];
    return numStr;
}

#pragma mark - private NSNull
+ (NSString *)nullString{
    return @"";
}

#pragma mark - private NSString
+ (NSString *)stringToString:(NSString *)string{
    return string;
}

@end

