//
//  HMAlertView.h
//  Demo
//
//  Created by lijunfeng on 2016/10/22.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HMAlertClickBlock)(NSInteger buttonIndex);

@interface HMAlertView : UIView

+ (void)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancel;

@end
