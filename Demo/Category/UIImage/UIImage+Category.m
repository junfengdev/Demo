//
//  UIImage+Category.m
//  Demo
//
//  Created by lijunfeng on 2016/10/16.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "UIImage+Category.h"

@implementation UIImage (Category)

- (UIImage *)cornerImageWithSize:(CGSize)size fillColor:(UIColor *)fillColor 
{
    // 1.利用绘图，建立上下文
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    // 2.设置填充颜色
    [fillColor setFill];
    UIRectFill(rect);
    
    // 3.利用贝塞尔路径裁剪效果
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(10, 10)];
    
    [path addClip];
    
    // 4.绘制图像
    [self drawInRect:rect];
    
    // 5.取得结果
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
//    NSTimeInterval start = CACurrentMediaTime();
//    NSLog(@"%f",CACurrentMediaTime()-start);

    return result;
}


/// 在iOS开发中，block回调用处最多的就是异步完成之后，通过参数回调通知方法结果!!
- (void)cornerImageWithSize:(CGSize)size fillColor:(UIColor *)fillColor competion:(void (^)(UIImage *image))competion
{
//    NSTimeInterval start = CACurrentMediaTime();
//    NSLog(@"%f",CACurrentMediaTime()-start);

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // 1.利用绘图，建立上下文
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
        
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        
        // 2.设置填充颜色
        [fillColor setFill];
        UIRectFill(rect);
        
        // 3.利用贝塞尔路径裁剪效果
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(10, 10)];
        
        [path addClip];
        
        // 4.绘制图像
        [self drawInRect:rect];
        
        // 5.取得结果
        UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (competion != nil) {
                competion(result);
            }
            
        });
    });
}

@end
