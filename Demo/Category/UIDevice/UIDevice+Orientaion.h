//
//  UIDevice+Orientaion.h
//  Demo
//
//  Created by admin on 16/9/12.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface UIDevice (Orientaion)

- (float) orientationAngleRelativeToOrientation:(UIDeviceOrientation) someOrientation;
+ (NSString *) orientationString: (UIDeviceOrientation) orientation;

@property (nonatomic, readonly) BOOL isLandscape;
@property (nonatomic, readonly) BOOL isPortrait;
@property (nonatomic, readonly) NSString *orientationString;
@property (nonatomic, readonly) float orientationAngle;
@property (nonatomic, readonly) UIDeviceOrientation acceleratorBasedOrientation;

@end
