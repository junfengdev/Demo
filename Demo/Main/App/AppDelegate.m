//
//  AppDelegate.m
//  News
//
//  Created by junFung on 15/9/25.
//  Copyright © 2015年 yc. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabBarController.h"
#import "AdvertiseManager.h"
#import "AdvertiseView.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[MainTabBarController alloc] init];
    
    [self.window makeKeyAndVisible];
    
    [self getStaticImageOfAdvertising];
    
    return YES;
}

#pragma mark - 添加静态广告页面
- (void)getStaticImageOfAdvertising{
    
    NSString *oldImageName = [kUserDefaults objectForKey:adImageName];
    NSString *filePathBef = [AdvertiseManager getFilePathWithImageName:oldImageName];
    BOOL isExistBef = [AdvertiseManager isFileExistWithFilePath:filePathBef];
    
    if (isExistBef) {
        AdvertiseView *advertiseView = [[AdvertiseView alloc] initWithFrame:self.window.bounds launchType:AdLaunchProgressType];
        advertiseView.filePath = filePathBef;
        [advertiseView show];
    }else{
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }
    
    NSString *imageUrl = @"http://a.hiphotos.baidu.com/image/pic/item/b21c8701a18b87d6ff2ca7bc030828381f30fd23.jpg";
    
    NSArray *stringArr = [imageUrl componentsSeparatedByString:@"/"];
    NSString *imageName = stringArr.lastObject;
    
    NSString *filePath = [AdvertiseManager getFilePathWithImageName:imageName];
    BOOL isExist = [AdvertiseManager isFileExistWithFilePath:filePath];
    // 如果该图片不存在，则删除老图片，下载新图片
    if (!isExist){
        [AdvertiseManager downloadAdImageWithUrl:imageUrl imageName:imageName];
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
