//
//  HomeViewController.m
//  Demo
//
//  Created by admin on 16/8/30.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HomeViewController.h"
#import "RunTimeController.h"
#import "AutoLayoutController.h"
#import "TestSuperViewController.h"
#import "CornerController.h"
#import "CircleScrollViewController.h"
#import "InterViewQuestionsController.h"
#import "UserDefualtViewController.h"
#import "GCDTestViewController.h"

@interface HomeViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

/**  里面存放的是控制器的名称*/
@property (nonatomic, strong) NSArray *controllerNames;


@end

@implementation HomeViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]init];
        _tableView.showsVerticalScrollIndicator = NO;
        
    }
    return _tableView;
}

#pragma mark -----tableView delegate && tableView dataSource -------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.controllerNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (!IsArrEmpty(self.controllerNames)) {
        cell.textLabel.text = [self controllerNames][indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *controller = [self viewControllers][indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}

- (NSArray *)controllerNames{
    if (!_controllerNames) {
        _controllerNames = @[@"RunTime",@"AutoLayout",@"Self,Super,Class",@"图片圆角",@"轮播图",@"面试集锦",@"UserDefaults",@"GCD"];
    }
    return _controllerNames;
}

- (NSArray *)viewControllers{
    
    NSArray *arr = @[@"RunTimeController",@"AutoLayoutController",@"TestSuperViewController",@"CornerController",@"CircleScrollViewController",@"InterViewQuestionsController",@"UserDefualtViewController",@"GCDTestViewController"];
    
    NSMutableArray * viewControlers = [NSMutableArray arrayWithArray:arr];
    
    for (NSUInteger i = 0; i < viewControlers.count; i ++) {
        
        NSString * controllerName = viewControlers[i];
        
        UIViewController * vc = [[NSClassFromString(controllerName) alloc] init];
        
        [viewControlers replaceObjectAtIndex:i withObject:vc];
    }
    
    return viewControlers;
}

@end
