//
//  AllListViewController.m
//  Demo
//
//  Created by admin on 16/8/19.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "AllListViewController.h"
#import "SocialViewController.h"
#import "LFLiveKitViewController.h"
#import "HMNavigationController.h"
#import "HMTabBarController.h"
#import "AVPlayViewController.h"
#import "HMReadyLiveViewController.h"

@interface AllListViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

/**  里面存放的是控制器的名称*/
@property (nonatomic, strong) NSArray *controllers;

@end

@implementation AllListViewController

- (NSArray *)controllers{
    if (!_controllers) {
        _controllers = @[@"Social",@"LFLiveKit"];
    }
    return _controllers;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"HuoMaoLive" style:UIBarButtonItemStyleDone target:self action:@selector(onClickLiveButton)];
    
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]init];
        _tableView.showsVerticalScrollIndicator = NO;
    }
    return _tableView;
}

#pragma mark -----tableView delegate && tableView dataSource -------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.controllers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (!IsArrEmpty(self.controllers)) {
        cell.textLabel.text = self.controllers[indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *controller = nil;
    
    switch (indexPath.row)
    {
        case 0:
            controller = [[SocialViewController alloc] initWithTitle:self.controllers[indexPath.row]];
            break;
        
        case 1:
            controller = [[LFLiveKitViewController alloc] initWithTitle:self.controllers[indexPath.row]];
            break;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)onClickLiveButton{
    
    HMTabBarController *controller = [[HMTabBarController alloc] init];
    // 设置只有竖屏进入
    [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    [self presentViewController:controller animated:YES completion:nil];

}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}

@end
