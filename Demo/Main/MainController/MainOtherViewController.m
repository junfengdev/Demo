//
//  MainOtherViewController.m
//  Demo
//
//  Created by admin on 16/9/29.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "MainOtherViewController.h"
#import "SwiftNavigationController.h"
#import "SwiftTabBarController.h"

@interface MainOtherViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

/**  里面存放的是控制器的名称*/
@property (nonatomic, strong) NSArray *controllerNames;

@end

@implementation MainOtherViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]init];
        _tableView.showsVerticalScrollIndicator = NO;
        
    }
    return _tableView;
}

#pragma mark -----tableView delegate && tableView dataSource -------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.controllerNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"MainOtherViewControllerCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (!IsArrEmpty(self.controllerNames)) {
        cell.textLabel.text = [self controllerNames][indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if (indexPath.row == 0) {
        
        //将我们的storyBoard实例化，“Main”为StoryBoard的名称
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"SwiftStoryboard" bundle:nil];
        
        //将第二个控制器实例化，"SwiftTabBarController"为我们设置的控制器的ID
        SwiftTabBarController *SwiftTabBarController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwiftTabBarController"];
        
        //跳转事件
        [self presentViewController:SwiftTabBarController animated:YES completion:nil];
    }
    else{
        UIViewController *controller = [self viewControllers][indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (NSArray *)controllerNames{
    if (!_controllerNames) {
        _controllerNames = @[@"Swift"];
    }
    return _controllerNames;
}

- (NSArray *)viewControllers{
    
    NSMutableArray * viewControlers = [NSMutableArray arrayWithArray:@[@"SwiftTabBarController"]];
    
    for (NSUInteger i = 0; i < viewControlers.count; i ++) {
        
        NSString * controllerName = viewControlers[i];
        
        UIViewController * vc = [[NSClassFromString(controllerName) alloc] init];
        
        [viewControlers replaceObjectAtIndex:i withObject:vc];
    }
    
    return viewControlers;
}



@end

