//
//  WebImageManager.h
//  TextAndImage
//
//  Created by lijunfeng on 16/11/7.
//  Copyright © 2016年 lvzilong.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebImageManager : NSObject

+ (void)imageUrl:(NSString *)imageUrl complete:(void(^)(UIImage *imageCache))complete;

+ (void)clearImageCache;

@end
