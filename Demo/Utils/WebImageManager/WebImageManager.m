//
//  WebImageManager.m
//  TextAndImage
//
//  Created by lijunfeng on 16/11/7.
//  Copyright © 2016年 lvzilong.com. All rights reserved.
//

#define webImageCachePath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/WebImageCache"]

#import "WebImageManager.h"

@implementation WebImageManager

+ (void)initialize{
    
    NSLog(@"%@",webImageCachePath);
    
    // 判断文件夹是否存在，如果不存在，则创建
    if (![[NSFileManager defaultManager] fileExistsAtPath:webImageCachePath]) {
        NSLog(@"FileDir is not exists.");

        [[NSFileManager defaultManager] createDirectoryAtPath:webImageCachePath withIntermediateDirectories:YES attributes:nil error:nil];
    } else {
        NSLog(@"FileDir is exists.");
    }
}

// 把图片直接保存到沙盒中，然后再把路径存储起来，等到用图片的时候先获取图片的路径，再通过路径拿到图片
+ (void)imageUrl:(NSString *)imageUrl complete:(void(^)(UIImage *imageCache))complete{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSArray *stringArr = [imageUrl componentsSeparatedByString:@"/"];
      
        if ([self isBlankArray:stringArr]) {
        
            NSString *imageName = stringArr.lastObject;
            
            // 如果缓存中有这个名字的图片直接返回无需下载
            if ([self getImageWithName:imageName]) {
                NSLog(@"有这个图片:%@",imageName);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (complete) {
                        complete([self getImageWithName:imageName]);
                    }
                });
            }
            else{
                NSLog(@"没有这个图片:%@",imageName);
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                
                UIImage *image = [UIImage imageWithData:data];
                
                NSString *imagePath = [self getFilePathWithImageName:imageName];
                
                //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
                BOOL sueecss = [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (sueecss == YES) {
                        if ([self getImageWithName:imageName]) {
                            if (complete) {
                                complete([self getImageWithName:imageName]);
                            }
                        }
                        else{
                            if (complete) {
                                complete(nil);
                            }
                        }
                    }
                    else{
                        if (complete) {
                            complete(nil);
                        }
                    }
                    
                });
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (complete) {
                    complete(nil);
                }
                
            });
        }
       
    });
}

#pragma mark - 从缓存中读取图片
+ (UIImage *)getImageWithName:(NSString *)imageName{
    NSString *imagePath = [self getFilePathWithImageName:imageName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        UIImage *image=[[UIImage alloc]initWithContentsOfFile:imagePath];
        if (image) {
            return image;
        }
    }
    return nil;
}

#pragma mark - 根据图片名拼接文件路径
+ (NSString *)getFilePathWithImageName:(NSString *)imageName
{
    if ([self isBlank:imageName] == NO) {
        NSString *filePath = [webImageCachePath stringByAppendingPathComponent:imageName];        
        return filePath;
    }
    return nil;
}

#pragma mark - 判断字符串是否是空
+ (BOOL)isBlank:(NSString *)string{
    if (string == nil || string == NULL) {
        return YES;
    }else if ([string isEqualToString:@""]){
        return YES;
    }else if ([string isEqualToString:@"(null)"]){
        return YES;
    }else if([string isEqualToString:@"null"]){
        return YES;
    }else if ([string isEqualToString:@" "]){
        return YES;
    }else if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL)isBlankArray:(NSArray *)array{
    if (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0) {
        return YES;
    }else{
        return YES;
    }
}

+ (void)clearImageCache
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:webImageCachePath]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:webImageCachePath error:&error];
    }
}

@end
