//
//  ShowManager.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "ShowManager.h"
#import "MBProgressHUD.h"

@implementation ShowManager

// 显示提示内容
+ (void)showHint:(NSString *)hint WithView:(UIView *)view{
    
    if (!IsStrEmpty(hint)) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = hint;
        hud.margin = 10.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:1.f];        
    }
}


@end
