//
//  ShowManager.h
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowManager : NSObject
+ (void)showHint:(NSString *)hint WithView:(UIView *)view;
@end
