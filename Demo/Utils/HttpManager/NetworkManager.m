//
//  NetworkManager.m
//  HuoMao
//
//  Created by admin on 16/8/4.
//  Copyright © 2016年 woo. All rights reserved.
//

#import "NetworkManager.h"
#import "HttpEncryptHelper.h"
#import <AFHTTPSessionManager.h>

@implementation NetworkManager

+ (instancetype)sharedInstance
{
    static NetworkManager * _instace = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instace = [[self alloc] init];
    });
    return _instace;
}

- (void)getNetworkStatusUnknown:(NetUnknown)unknown
                   notReachable:(NetNotReachable)notReachable
               reachableViaWWAN:(NetReachableViaWWAN)reachableViaWWAN
               reachableViaWiFi:(NetReachableViaWiFi)reachableViaWiFi
{
    // 创建网络监测者
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 监测到不同网络的情况
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
                unknown();
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
                notReachable();
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                reachableViaWWAN();
                
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                reachableViaWiFi();
                break;
                
            default:
                break;
        }
    }] ;
    
    // 开始监听网络状况
    [manager startMonitoring];
}


- (void)GET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(RequestSuccess)success failure:(RequestFailed)failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
   
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"text/plain"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = (self.acceptableContentTypes?self.acceptableContentTypes:contentTypes);
    
    manager.requestSerializer.timeoutInterval = (self.timeoutInterval?self.timeoutInterval:kTimeoutInterval);
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSString *secretURL = [HttpEncryptHelper getEncryptedURL:URLString andPamraters:parameters];
    
    [manager GET:secretURL parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            NSError *jsonError;
            id obj = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&jsonError];
            success(obj);
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

- (void)POST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(RequestSuccess)success failure:(RequestFailed)failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"text/plain"];
    [contentTypes addObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = (self.acceptableContentTypes?self.acceptableContentTypes:contentTypes);
    manager.requestSerializer.timeoutInterval = (self.timeoutInterval ? self.timeoutInterval:kTimeoutInterval);
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSString *secretURL = [HttpEncryptHelper getEncryptedURL:URLString andPamraters:parameters];
    
    [manager POST:secretURL parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                success(responseObject);
            });
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            if (error.code) {
                [self showError:error.code];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                failure(error);
            });
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

- (void)showError:(NSInteger)code{
    if (code == -1001) {
        [ShowManager showHint:@"请求超时" WithView:WINDOW];
    }
    if (code == -1005) {
        [ShowManager showHint:@"网络连接已中断" WithView:WINDOW];
    }
    if (code == -1009) {
        [ShowManager showHint:@"网络已断开" WithView:WINDOW];
    }
}

@end




@implementation NetworkHelper

static AFHTTPSessionManager * _manager;

#pragma mark - 初始化AFHTTPSessionManager相关属性
/**
 *  所有的HTTP请求共享一个AFHTTPSessionManager,原理参考地址:http://www.jianshu.com/p/5969bbb4af9f
 *  + (void)initialize该初始化方法在当用到此类时候只调用一次
 */
+ (void)initialize
{
    _manager = [AFHTTPSessionManager manager];
    
    //设置请求参数的类型:JSON (AFJSONRequestSerializer,AFHTTPRequestSerializer)
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    //设置请求的超时时间
    _manager.requestSerializer.timeoutInterval = 15.f;
    
    //设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*", nil];
}

+ (NSURLSessionTask *)GET:(NSString *)url parameters:(NSDictionary *)parameters success:(SuccessHanlder)success failed:(FailedHanlder)failed{
    
    NSString *encryptedURL = [HttpEncryptHelper getEncryptedURL:url andPamraters:parameters];
    
    return [_manager GET:encryptedURL parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                success(responseObject);
            });
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                failed(error);
            });
        }
    }];
}



@end
