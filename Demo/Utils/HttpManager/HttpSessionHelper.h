//
//  HttpSessionHelper.h
//  Demo
//
//  Created by lijunfeng on 2016/10/23.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^Success)(NSDictionary *responseObject);
typedef void(^Failed)(NSError *error);

@interface HttpSessionHelper : NSObject

/// GET请求

+ (__kindof NSURLSessionTask *)GET:(NSString *)url
                        parameters:(NSDictionary *)parameters
                           success:(Success)success
                            failed:(Failed)failed;

@end
