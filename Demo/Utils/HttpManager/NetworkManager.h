//
//  NetworkManager.h
//  HuoMao
//
//  Created by admin on 16/8/4.
//  Copyright © 2016年 woo. All rights reserved.
//  封装网络请求库

// 请求失败处理
typedef NS_ENUM (NSInteger , RequestFailHandler)
{
    requestTimeOut = 0  ,  //请求超时 －1001
    networkBroken       ,  // 网络已断开 －1009
    networkDisconnected ,  //网路连接已中断 －1005
};

#import <Foundation/Foundation.h>

typedef void (^ RequestSuccess)(id responseObject);     // 成功Block
typedef void (^ RequestFailed)(NSError *error);        // 失败Blcok

typedef void (^ NetUnknown)();          // 未知网络状态的Block
typedef void (^ NetNotReachable)();     // 无网络的Blcok
typedef void (^ NetReachableViaWWAN)(); // 蜂窝数据网的Block
typedef void (^ NetReachableViaWiFi)(); // WiFi网络的Block
typedef void (^ Progress)(NSProgress * progress); // 上传或者下载进度Block

@interface NetworkManager : NSObject

+ (instancetype)sharedInstance;

/**
 *  超时时间(默认20秒)
 */
@property (nonatomic, assign) NSTimeInterval timeoutInterval;

/**
 *  可接受的响应内容类型
 */
@property (nonatomic, copy) NSSet <NSString *> *acceptableContentTypes;

/**
 *  网络监测
 *
 *  @param unknown          未知网络
 *  @param notReachable     无网络
 *  @param reachableViaWWAN 蜂窝数据网
 *  @param ReachableViaWiFi WiFi网络
 */
- (void)getNetworkStatusUnknown:(NetUnknown)unknown
                   notReachable:(NetNotReachable)notReachable
               reachableViaWWAN:(NetReachableViaWWAN)reachableViaWWAN
               reachableViaWiFi:(NetReachableViaWiFi)reachableViaWiFi;

/**
 *  封装的GET请求
 *
 */
- (void)GET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(RequestSuccess)success failure:(RequestFailed)failure;

/**
 *  封装的POST请求
 *
 */
- (void)POST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(RequestSuccess)success failure:(RequestFailed)failure;




@end


#pragma mark - AF3.x重新封装 添加网络自动缓存功能 - 10.19号

typedef void(^SuccessHanlder)(id responseObject);
typedef void(^FailedHanlder)(NSError *error);

@interface NetworkHelper : NSObject

/// GET请求 ,无缓存
+ (__kindof NSURLSessionTask *)GET:(NSString *)url
                        parameters:(NSDictionary *)parameters
                           success:(SuccessHanlder)success
                            failed:(FailedHanlder)failed;



@end
