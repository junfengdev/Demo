//
//  HttpEncryptHelper.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HttpEncryptHelper.h"
#import <CommonCrypto/CommonDigest.h>

@implementation HttpEncryptHelper

- (void)encryptedURLAndGetData:(NSString *)url andPamraters:(NSDictionary *)paramters andTarget:(id)target{
    NSURL *requestUrl = [NSURL URLWithString:[HttpEncryptHelper getEncryptedURL:url andPamraters:paramters]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:target];
    [connection start];
}

// 拼接字符串，保证安全性
+ (NSString*)getEncryptedURL:(NSString *)url andPamraters:(NSDictionary*)paramters{
    
    NSMutableString *resultURL = [[NSMutableString alloc] initWithString:url];
    NSMutableArray *keyArr = [[NSMutableArray alloc] init];
    NSEnumerator *keyEnmuer = [paramters keyEnumerator];
    NSString *key = [keyEnmuer nextObject];
    [keyArr addObject:@"time"];
    while (key != nil) {
        [keyArr addObject:key];
        key = [keyEnmuer nextObject];
    }
    NSArray *sortedArr = [keyArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return 0 - [obj1 compare:obj2];
    }];
    
    uint timeStamp = [[NSDate date] timeIntervalSince1970];
    
    /**** 将时间戳格式到分钟 ****/
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:timeStamp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];//设定时间格式,去掉秒、只到分钟
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    
    NSDate *currentDate = [dateFormatter dateFromString:currentDateStr];
    
    uint timeStampjson = [currentDate timeIntervalSince1970];
    /********/
    
    NSMutableString *valueListURL =[[NSMutableString alloc] init];
    
    for (NSString *str in sortedArr){
        if([str isEqual: @"time"]){
            [resultURL appendString:@"&time="]; //&time=
            [resultURL appendFormat:@"%i",timeStampjson];//timeStamp
            [valueListURL appendFormat:@"%i",timeStampjson];//timeStamp
            continue;
        }
        [resultURL appendString:@"&"];
        [resultURL appendString:str];
        [resultURL appendString:@"="];
        NSString* param = [NSString stringWithFormat:@"%@",[paramters objectForKey:str]];
        if(param != nil){
            param = [param stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [resultURL appendString:param];
        }
        [valueListURL appendFormat:@"%@",[paramters objectForKey:str]];
    }
    
    [valueListURL appendFormat:@"%@",kVariKey];
    [resultURL appendString:@"&token="];
    [resultURL appendFormat:@"%@",[self createMD5:valueListURL]];
    
    
    // 对接口?&拼在一起做处理
    NSString *strUrl = resultURL;
    if ([strUrl rangeOfString:@"?&"].location != NSNotFound) { //如果包含?& ->则替换为?
        strUrl = [resultURL stringByReplacingOccurrencesOfString:@"?&" withString:@"?"];
    }
    
    return strUrl;
}

// MD5加密
+(NSString *)createMD5:(NSString *)signString
{
    const char *cStr = [signString UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest );
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

@end
