//
//  HMHttpManager.h
//  HuoMao
//
//  Created by 李俊峰 on 16/6/28.
//  Copyright © 2016年 woo. All rights reserved.
//

typedef void(^SuccessBlock)(NSDictionary *dict);
typedef void(^ErrorBlock)(NSError *error);

#import <Foundation/Foundation.h>

@interface HMHttpManager : NSObject
/**
 *  版本更新提示
 */
+ (void)getUpdate:(SuccessBlock)successBack error:(ErrorBlock)errorBack;

/**
 *  首页banner图
 */
+ (void)getBannerImages:(SuccessBlock)successBack error:(ErrorBlock)errorBack;

/**
 *  首页推荐
 */
+ (void)getHomeRecommend:(SuccessBlock)successBack error:(ErrorBlock)errorBack;

/**
 *  首页频道推荐
 */
+ (void)getHomeRecGames:(SuccessBlock)successBack error:(ErrorBlock)errorBack;

@end
