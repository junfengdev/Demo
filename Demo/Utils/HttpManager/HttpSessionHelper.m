//
//  HttpSessionHelper.m
//  Demo
//
//  Created by lijunfeng on 2016/10/23.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HttpSessionHelper.h"
#import "HttpEncryptHelper.h"

@implementation HttpSessionHelper

/**
 * get:1、参数拼接
 * // 遍历参数字典,一一取出参数,按照参数格式拼接在 url 后面.
 */

+ (NSURLSessionTask *)GET:(NSString *)url parameters:(NSDictionary *)parameters success:(Success)success failed:(Failed)failed{
    
    // 实例化一个对话
    NSURLSession *session = [NSURLSession sharedSession];

    NSString *secretStr = [HttpEncryptHelper getEncryptedURL:url andPamraters:parameters];
    
    NSLog(@"GET==>%@",secretStr);
    
    // 创建请求对象
    NSURL *secretURL = [NSURL URLWithString:secretStr];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:secretURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:15];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil) {
            NSError *jsonError;
            id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            if (success != nil) {
                if ([obj isKindOfClass:[NSDictionary class]]) {
                    success(obj);
                }else{
                    success(nil);
                }                
            }
        }
        else{
            failed(error);
        }
    }];
    
    [task resume];
    return task;
}

@end
