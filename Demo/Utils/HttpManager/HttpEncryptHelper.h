//
//  HttpEncryptHelper.h
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpEncryptHelper : NSObject

- (void)encryptedURLAndGetData:(NSString *)url andPamraters:(NSDictionary*)paramters andTarget:(id)target;

+ (NSString*)getEncryptedURL:(NSString *)url andPamraters:(NSDictionary*)paramters;

+ (NSString *)createMD5:(NSString *)signString;

@end
