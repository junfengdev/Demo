//
//  HMHttpManager.m
//  HuoMao
//
//  Created by 李俊峰 on 16/6/28.
//  Copyright © 2016年 woo. All rights reserved.
//

#import "HMHttpManager.h"
#import "NetworkManager.h"
#import "HttpConstant.h"
#import "HMGTMBase64.h"
#import "HttpEncryptHelper.h"

#import "HttpSessionHelper.h"

@implementation HMHttpManager

+ (void)getUpdate:(SuccessBlock)successBack error:(ErrorBlock)errorBack;
{
    NSString *url = [HttpConstant get_api_getUpdate];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:Platform              forKey:@"refer"];
    
    [[NetworkManager sharedInstance] GET:url parameters:params success:^(id responseObject) {
        successBack(responseObject);
    } failure:^(NSError *error) {
        errorBack(error);
    }];
}

+ (void)getBannerImages:(SuccessBlock)successBack error:(ErrorBlock)errorBack;
{
    NSString *url = [HttpConstant get_api_getBanner];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:Platform              forKey:@"refer"];
    
    [HttpSessionHelper GET:url parameters:params success:^(id responseObject) {
        successBack(responseObject);
    } failed:^(NSError *error) {
        errorBack(error);
    }];
}

+ (void)getHomeRecommend:(SuccessBlock)successBack error:(ErrorBlock)errorBack;
{
    NSString *url = [HttpConstant get_api_getRecmmend];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:Platform              forKey:@"refer"];

    [HttpSessionHelper GET:url parameters:params success:^(id responseObject) {
        successBack(responseObject);
    } failed:^(NSError *error) {
        errorBack(error);
    }];
}

+ (void)getHomeRecGames:(SuccessBlock)successBack error:(ErrorBlock)errorBack;
{
    NSString *url = [HttpConstant get_api_get_rec_games];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:Platform              forKey:@"refer"];
    
    [HttpSessionHelper GET:url parameters:params success:^(id responseObject) {
        successBack(responseObject);
    } failed:^(NSError *error) {
        errorBack(error);
    }];
}

#pragma mark - openID
// 计算openid
+ (NSString *)getOpenid:(NSString *)uid
{
    /************ 新版添加mp_openid验证 ************/
    // openid验证
    // mp_openid生成规则：md5(uid+公钥+时间戳)
    // 1、time 2、token 3、mp_openid
    /*********************************************/
    
    uint timeStamp = [[NSDate date] timeIntervalSince1970];
    
    /**** 将时间戳格式到分钟 ****/
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:timeStamp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];//设定时间格式,去掉秒、只到分钟
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    
    NSDate *currentDate = [dateFormatter dateFromString:currentDateStr];
    
    uint timeStampjson = [currentDate timeIntervalSince1970];
    /**** 将时间戳格式到分钟 ****/
    
    NSString *openid = [NSString stringWithFormat:@"%@%@%d",uid,kVariKey,timeStampjson];
    
    NSString *secretOpenid = [HttpEncryptHelper createMD5:openid];
    
    return secretOpenid;
    
}
@end
