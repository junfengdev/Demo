//
//  ShareManager.h
//  Demo
//
//  Created by lijunfeng on 16/9/7.
//  Copyright © 2016年 hm. All rights reserved.
//  SharesdkManager

#import <Foundation/Foundation.h>

#import <ShareSDK/ShareSDK.h>

typedef NS_ENUM(NSUInteger, ShareContentType){
    /**
     *  自动适配类型，视传入的参数来决定
     */
    ShareContentTypeAuto         = 0,
    
    /**
     *  文本
     */
    ShareContentTypeText         = 1,
    
    /**
     *  图片
     */
    ShareContentTypeImage        = 2,
    
    /**
     *  网页
     */
    ShareContentTypeWebPage      = 3,
    
    /**
     *  应用
     */
    ShareContentTypeApp          = 4,
    
    /**
     *  音频
     */
    ShareContentTypeAudio        = 5,
    
    /**
     *  视频
     */
    ShareContentTypeVideo        = 6,
    
    /**
     *  文件类型(暂时仅微信可用)
     */
    ShareContentTypeFile         = 7
    
};

@interface ShareManager : NSObject

+(void)shareContentWithShareContentType:(ShareContentType)shareContentType
                           contentTitle:(NSString *)contentTitle
                     contentDescription:(NSString *)contentDescription
                           contentImage:(id)contentImage
                             contentURL:(NSString *)contentURL
                             showInView:(UIView *)showInView
                                success:(void (^)())success
                                failure:(void (^)(NSString *failureInfo))failure
                    OtherResponseStatus:(void (^)(SSDKResponseState state))otherResponseStatus;



@end
