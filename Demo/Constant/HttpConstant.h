//
//  HttpConstant.h
//  HuoMao
//
//  Created by 李俊峰 on 16/7/6.
//  Copyright © 2016年 woo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpConstant : NSObject
/**
 *  版本更新提示
 */
+ (NSString *)get_api_getUpdate;

/**
 *  Banner图
 */
+ (NSString *)get_api_getBanner;

/**
 *  首页热门推荐
 */
+ (NSString *)get_api_getRecmmend;

/**
 *  首页频道推荐
 */
+ (NSString *)get_api_get_rec_games;

/**
 *  游戏分类
 */
+ (NSString *)get_api_getGameList;

/**
 *  首页+号游戏分类
 */
+ (NSString *)get_api_get_games;

/**
 *  所有频道（直播）
 */
+ (NSString *)get_api_channelpage;

/**
 *  单个游戏频道
 */
+ (NSString *)get_api_channelPageWithCid;

/**
 *  搜索
 */
+ (NSString *)get_api_searchList;

/**
 *  设置用户订阅游戏类型(首页用户订阅哪些)
 */
+ (NSString *)get_api_setSubGame;

/**
 *  用户信息
 */
+ (NSString *)get_api_getUserInfo;

/**
 *  用户登录
 */
+ (NSString *)get_api_getUserLogin;

/**
 *  QQ登录
 */
+ (NSString *)get_api_QQLogin;

/**
 *  用户注册
 */
+ (NSString *)get_api_user_reg;

/**
 *  极验验证（1、获取三个参数）
 */
+ (NSString *)get_api_geet_response_str;

/**
 *  极验二验（2、二次验证）
 */
+ (NSString *)get_api_geet_success_validate;

/**
 *  发送验证码之前的检测
 */
+ (NSString *)get_api_checkSingleField;

/**
 *  注册之前发送手机验证码（不需要传type）
 */
+ (NSString *)get_api_mobilePostApi;

/**
 *  直播间-详情
 */
+ (NSString *)get_api_channelDetail;

/**
 *  聊天室-连接聊天室获取token
 */
+ (NSString *)get_api_getToken;

/**
 *  聊天室-发言
 */
+ (NSString *)get_api_sendMsg;

/**
 *  礼物-详情
 */
+ (NSString *)get_api_getGiftInfo;

/**
 *  礼物-送礼
 */
+ (NSString *)get_api_sendGift;

/**
 *  礼物-送豆
 */
+ (NSString *)get_api_sendFreeBean;

/**
 *  户外直播间-修改房间名
 */
+ (NSString *)get_api_editRoomInfo;

/**
 *  户外直播间-修改户外直播间类型
 */
+ (NSString *)get_api_updateOutdoorLive;

/**
 *  用户信息-修改用户昵称和头像
 */
+ (NSString *)get_api_userSetting;

/**
 *  用户信息-修改用户昵称
 */
+ (NSString *)get_api_userChecknickname;

/**
 *  用户信息-上传用户头像
 */
+ (NSString *)get_api_userUploadMobileImg;

/**
 *  用户信息-修改密码
 */
+ (NSString *)get_api_editPassword;

/**
 *  用户信息-充值-预订单
 */
+ (NSString *)get_api_generateAppleOrder;

/**
 *  用户信息-充值-去充值
 */
+ (NSString *)get_api_notifyApplePay;

/**
 *  用户信息-兑换猫币
 */
+ (NSString *)get_api_exchangePay;

/**
 *  用户信息-密码找回-手机找回
 */
+ (NSString *)get_api_userFindPwdWithPhone;

/**
 *  用户信息-密码找回-邮箱找回
 */
+ (NSString *)get_api_userFindPwdWithEmail;

/**
 *  直播间-订阅&取消订阅
 */
+ (NSString *)get_api_doSubscribe;

/**
 *  直播间-直播间里面的房间推荐
 */
+ (NSString *)get_api_getRec;

/**
 *  直播间-领取仙豆
 */
+ (NSString *)get_api_minTaskGet;

/**
 *  个人中心－消息列表
 */
+ (NSString *)get_api_messageList;

/**
 *  个人中心-任务列表
 */
+ (NSString *)get_api_getAllTaskStat;

/**
 *  个人中心-任务-领取豆子
 */
+ (NSString *)get_api_getTaskFreeBean;

/**
 *  个人中心-签到
 */
+ (NSString *)get_api_userSignIn;

/**
 *  个人中心-我与主播
 */
+ (NSString *)get_api_userAnchor;

/**
 *  个人中心-订阅列表
 */
+ (NSString *)get_api_usersSubscribe;

/**
 *  个人中心-充值列表
 */
+ (NSString *)get_api_showMemberrechargeRecord;

/**
 *  个人中心－消费列表
 */
+ (NSString *)get_api_showMemberpayRecord;

/**
 *  个人中心－最近观看
 */
+ (NSString *)get_api_channelIsLive;

/**
 *  个人中心－开播提醒
 */
+ (NSString *)get_api_getGuanzhu;

/**
 *  个人中心－开播提醒－设置是否开播
 */
+ (NSString *)get_api_setGuanzhu;

/**
 *  个人中心－帮助反馈提交
 */
+ (NSString *)get_api_submitFeedback;

/**
 *  直播间－开播提醒（提示条）
 */
+ (NSString *)get_api_liveRemind;

/**
 *  直播间-粉丝榜-总榜
 */
+ (NSString *)get_api_getRankListAll;

/**
 *  直播间-粉丝榜-周榜
 */
+ (NSString *)get_api_getRankListWeek;

/**
 *  个人中心－我与主播－开启粉丝勋章
 */
+ (NSString *)get_api_setFanMedal;

/**
 *  个人中心－我与主播－关闭粉丝勋章
 */
+ (NSString *)get_api_delFanMedal;

/**
 *  用户信息-绑定手机
 */
+ (NSString *)get_api_updateMobile;

/**
 *  用户信息-QQ登陆之后需要 绑定用户名和密码
 */
+ (NSString *)get_api_bindUserNameAndPwd;

/**
 *  用户信息-QQ登陆之后需要 绑定密码
 */
+ (NSString *)get_api_updateUserpwd;

/**
 *  聊天室封禁-用户禁言状态
 */
+ (NSString *)get_api_chatRoomUserStatus;

/**
 *  主播开播-获取流地址
 */
+ (NSString *)get_api_getStream;
+ (NSString *)get_api_getGagUserList;

/**
 *  聊天室封禁-举报
 */
+ (NSString *)get_api_report;

/**
 *  聊天室封禁-设置房管
 */
+ (NSString *)get_api_setRoomAdministrator;

/**
 *  聊天室封禁-取消房管
 */
+ (NSString *)get_api_delRoomAdministrator;

/**
 *  聊天室封禁-禁言
 */
+ (NSString *)get_api_setCommChannelGag;

/**
 *  聊天室封禁-取消禁言
 */
+ (NSString *)get_api_delCommUserGag;

/**
 *  户外直播间-修改当前分类（所有分类）
 */
+ (NSString *)get_api_getgamekinds;

/**
 *  户外直播间-保存设置推流
 */
+ (NSString *)get_api_pushStreamForMobile;
@end
