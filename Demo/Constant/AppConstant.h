
//
//  AppConstant.h
//  HuoMao
//
//  Created by 李俊峰 on 16/7/6.
//  Copyright © 2016年 woo. All rights reserved.
//

#ifndef AppConstant_h
#define AppConstant_h

#define kVariKey  @"EU*T*)*(#23ssdfd"                 //客户端和服务端验证使用的公钥

//#define HMConstIP @"http://tuyu.new.huomaotv.com.cn"  // 新框架接口域名
//#define HMConstIP @"http://test1.api.huomao.com"      // 预发布环境
#define HMConstIP @"http://api.huomao.com"          // 线上环境
//#define HMConstIP @"http://qa.new.huomaotv.com.cn"
//#define HMConstIP @"http://test.ios.api.huomao.com"

//
#define Platform  @"ios"        // 平台

#pragma mark - SDK

// 第三方
#define kShareSDK      @"7626d9da4d48"

#define sinaKey        @"2761075223"
#define sinaSecret     @"e54db4cf7430d6ae325a916152ba84d8"

#define qqKey          @"101138302"
#define qqSecret       @"5b949b288847780d83ff9cbc1b2934d8"

#define weixinAppID    @"wx2eb22409502e0408"
#define weixinKey      @"f1234d16470c070ba7353bfd0f1c397e"


// 极验验证
//#define kJiYan_CAPTCHA_ID @"b5d82792c078b1372b2d4aebe4d8ced4"//线上
#define kJiYan_CAPTCHA_ID @"f74f23cc35746a89c3020937538fcc4f"//tuyu测试

#pragma mark - Pubilc

// 限定请求重复时间限制
#define kRequestTimeLimitLocal  @"30"
#define kRequestTimeLimitServer @"kRequestTimeLimitServer"

// 网络请求超时时间
#define kTimeoutInterval 15

// 设置圆角的大小
#define kCornerRadius 5

#pragma mark - Default

// UserManager 用户信息

#define kName              @"kName"
#define kUserID            @"kUserID"
#define kUserCid           @"kUserCid"
#define kUserName          @"kUserName"
#define kAvatarUrl         @"kAvatarUrl"
#define kMoneyOne          @"kMoneyOne"
#define kMoneyTwo          @"kMoneyTwo"
#define kMobileNumber      @"kMobileNumber"
#define kUserNextLVExp     @"kUserNextLVExp"       // 距离下一次升级
#define kUserLeftFreeBean  @"kUserLeftFreeBean"    // 用户剩余多少仙豆
#define kUserLiveRemind    @"kUserLiveRemind"      // 个人中心开播提醒

// PlayerManager 直播间

#define kLiveStreamType       @"kLiveStreamType"    // 直播间清晰度类型
#define kSoftDecoding         @"kSoftDecoding"      // 软解码
#define kLivingRoomCids       @"kLivingRoomCids"    // 观看直播间的房间id(最近观看)
#define kLivingRoomBoxTime    @"kLivingRoomBoxTime" // 直播间宝箱时间
#define kUserLiveRemind       @"kUserLiveRemind"    // 个人中心开播提醒
#define kUserOutLiveFPS       @"kUserOutLiveFPS"   // 主播用户开播类型
#define kUserOutLiveBPS       @"kUserOutLiveBPS"   // 主播用户开播类型
#define kOutLiveStreamUrlType @"kOutLiveStreamUrlType" // 户外直播间清晰度类型
#define kRegularSleep         @"kRegularSleep"      // 定时休眠
#define kBarrageType          @"kBarrageType"       // 弹幕类型
#define kOutLiveScreenType    @"kOutLiveScreenType" // 户外直播间类型
#define kDMSwitchAction       @"kDMSwitchAction"    // 直播间弹幕是否打开
#define kRefreshStreamView    @"kRefreshStreamView" // 在Stream页面刷新


#pragma mark - PlayLive

#endif /* AppConstant_h */
