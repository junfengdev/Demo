//
//  HttpConstant.m
//  HuoMao
//
//  Created by 李俊峰 on 16/7/6.
//  Copyright © 2016年 woo. All rights reserved.
//

#import "HttpConstant.h"

@implementation HttpConstant

+ (NSString *)get_api_getUpdate{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getUpdate"];
}

+ (NSString *)get_api_getBanner{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getBanner"];
}

+ (NSString *)get_api_getRecmmend{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getRecmmend"];
}

+ (NSString *)get_api_get_rec_games{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/get_rec_games"];
}

+ (NSString *)get_api_channelpage{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/channelpage"];
}

+ (NSString *)get_api_channelPageWithCid{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/channelpage"];
}

+ (NSString *)get_api_searchList{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getSearchList"];
}

+ (NSString *)get_api_getUserInfo{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getUserinfo"];
}

+ (NSString *)get_api_getGameList{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getGameList"];
}

+ (NSString *)get_api_get_games{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/User/get_games"];
}

+ (NSString *)get_api_setSubGame{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/User/setSubGame"];
}

+ (NSString *)get_api_getUserLogin{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/user_login"];
}

+ (NSString *)get_api_QQLogin{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/qqconnectCallback"];
}

+ (NSString *)get_api_user_reg{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/user_reg"];
}

+ (NSString *)get_api_geet_response_str{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/geet_response_str"];
}

+ (NSString *)get_api_geet_success_validate{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/geet_success_validate"];
}

+ (NSString *)get_api_checkSingleField{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/checkSingleField"];
}

+ (NSString *)get_api_mobilePostApi{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/mobilePostApi"];
}

+ (NSString *)get_api_channelDetail{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/channelDetail"];
}

+ (NSString *)get_api_getToken{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getToken"];
}

+ (NSString *)get_api_sendMsg{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/sendmsg"];
}

+ (NSString *)get_api_getGiftInfo{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getGiftInfo"];
}

+ (NSString *)get_api_sendGift{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/sendGift"];
}

+ (NSString *)get_api_sendFreeBean{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/sendFreeBean"];
}

+ (NSString *)get_api_editRoomInfo{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/editRoomInfo"];
}

+ (NSString *)get_api_userSetting{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/userSetting"];
}

+ (NSString *)get_api_userChecknickname{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/checknickname"];
}

+ (NSString *)get_api_userUploadMobileImg{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/uploadMobileImg"];
}

+ (NSString *)get_api_updateOutdoorLive{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/updateOutdoorLive"];
}

+ (NSString *)get_api_generateAppleOrder{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/memberpayIOS/generateAppleOrder"];
}

+ (NSString *)get_api_notifyApplePay{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/Notifypay/notifyApplePay"];
}

+ (NSString *)get_api_exchangePay{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/exchangePay"];
}

+ (NSString *)get_api_userFindPwdWithPhone{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/findPwd"];
}

+ (NSString *)get_api_userFindPwdWithEmail{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/findPwd"];
}

+ (NSString *)get_api_doSubscribe{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/doSubscribe"];
}

+ (NSString *)get_api_getRec{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getRec"];
}

+ (NSString *)get_api_getAllTaskStat{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/task/getAllTaskStat"];
}

+ (NSString *)get_api_getTaskFreeBean{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/task/getTaskFreeBean"];
}

+ (NSString *)get_api_userSignIn{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/userSignIn"];
}

+ (NSString *)get_api_userAnchor{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/anchor"];
}

+ (NSString *)get_api_usersSubscribe{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getUsersSubscribe"];
}

+ (NSString *)get_api_showMemberrechargeRecord{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/showMemberrechargeRecord"];
}

+ (NSString *)get_api_messageList{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getMessageList"];
}

+ (NSString *)get_api_showMemberpayRecord{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/showMemberpayRecord"];
}

+ (NSString *)get_api_minTaskGet{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/task/minTaskGet"];
}

+ (NSString *)get_api_editPassword{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/edit_passwd"];
}

+ (NSString *)get_api_channelIsLive{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getChannelIsLive"];
}

+ (NSString *)get_api_getGuanzhu{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getGuanzhu"];
}

+ (NSString *)get_api_setGuanzhu{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/setGuanzhu"];
}

+ (NSString *)get_api_submitFeedback{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/submitFeedback"];
}

+ (NSString *)get_api_liveRemind{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/liveRemind"];
}

+ (NSString *)get_api_getRankListAll{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getRankListAll"];
}

+ (NSString *)get_api_getRankListWeek{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/channels/getRankListWeek"];
}

+ (NSString *)get_api_setFanMedal{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/setFanMedal"];
}

+ (NSString *)get_api_delFanMedal{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/delFanMedal"];
}

+ (NSString *)get_api_updateMobile{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/updateMobile"];
}

+ (NSString *)get_api_bindUserNameAndPwd{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/bindUserNameAndPwd"];
}

+ (NSString *)get_api_updateUserpwd{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/userlogin/updateUserpwd"];
}

+ (NSString *)get_api_getGagUserList{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/getGagUserList"];
}

+ (NSString *)get_api_getStream{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/stream/getStream"];
}

+ (NSString *)get_api_report{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/report"];
}

+ (NSString *)get_api_setRoomAdministrator
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/setRoomAdministrator"];
}

+ (NSString *)get_api_delRoomAdministrator
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/delRoomAdministrator"];
}

+ (NSString *)get_api_setCommChannelGag
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/setCommChannelGag"];
}

+ (NSString *)get_api_delCommUserGag
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/delCommUserGag"];
}

+ (NSString *)get_api_getgamekinds
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/gamekinds"];
}

+ (NSString *)get_api_pushStreamForMobile
{
    return [NSString stringWithFormat:@"%@%@?",HMConstIP,@"/user/pushStreamForMobile"];
}
@end
