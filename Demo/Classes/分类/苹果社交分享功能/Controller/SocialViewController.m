//
//  SocialViewController.m
//  Demo
//
//  Created by admin on 16/9/1.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "SocialViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import "ShareManager.h"

@interface SocialViewController ()

@end

@implementation SocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"shareSDK分享" style:UIBarButtonItemStyleDone target:self action:@selector(onClickShareSDK)];
}

- (void)test{
    if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeSinaWeibo]) {    NSLog(@"不可用");}SLComposeViewController *slVc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeSinaWeibo];[self presentViewController:slVc animated:YES completion:^{    }];[slVc setInitialText:@"默认文字"];[slVc addImage:[UIImage imageNamed:@"pic.png"]];
    // 利用block回调slVc.completionHandler = ^(SLComposeViewControllerResult result){        switch (result) {        case SLComposeViewControllerResultDone:            NSLog(@"发送成功");            break;        case SLComposeViewControllerResultCancelled:            NSLog(@"发送失败");            break;    }    };
}

- (void)sendSocialRequest
{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *type = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierSinaWeibo];
    [account requestAccessToAccountsWithType:type options:nil completion:^(BOOL granted, NSError *error) {
        if (granted)
        {
            NSArray *array = [account accountsWithAccountType:type];
            NSLog(@"账号%@",array);
            if (array.count > 0)
            {
                SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeSinaWeibo requestMethod:SLRequestMethodGET URL:[NSURL URLWithString:@"https://api.weibo.com/2/common/get_city.json"] parameters:[NSDictionary dictionaryWithObject:@"001011" forKey:@"province"]];
                request.account = [array objectAtIndex:0];
                [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    //NSLog(@" == %@",responseData);
                    NSError *errorr = nil;
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&errorr];
                    if (!error)
                    {
                        NSLog(@"北京区 %@",dic);
                    }
                }];
            }
        }
    }];
}

- (void)onClickShareSDK{
    [ShareManager shareContentWithShareContentType:ShareContentTypeAuto contentTitle:@"测试分享" contentDescription:@"李龙的技术分享博客 http://www.lilongcnc.cc" contentImage:[UIImage imageNamed:@"test4"] contentURL:@"http://lilongcnc.cc" showInView:self.view success:^{
        
        NSLog(@"分享成功");
    } failure:^(NSString *failureInfo) {
        
        NSLog(@"分享失败:%@",failureInfo);
        
    } OtherResponseStatus:^(SSDKResponseState state) {
        NSLog(@"分享异常类型");
    }];
    
    /**
     test1.png 1.2M 868*896 在QQ 中提示太大
     test2.png 328K 438*419 微信/微信分享只进入'SSDKResponseStateBegin'
     test3.png 117K 868*896 QQ/QQ空间 分享会提示图片太大
     test4.png 112K 200*206 微信/微信分享只进入'SSDKResponseStateBegin'
     这里涉及到平台分享规则:http://wiki.mob.com/%E5%B9%B3%E5%8F%B0%E7%89%B9%E6%AE%8A%E6%80%A7/
     */
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
