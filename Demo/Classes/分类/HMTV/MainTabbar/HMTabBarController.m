//
//  HMTabBarController.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMTabBarController.h"
#import "HMNavigationController.h"
#import "HMHomeController.h"
#import "MeViewController.h"
#import "HomeRecommandController.h"

@implementation HMTabBarController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addChildVC];
}

#pragma mark --- 添加子视图 ---
- (void)addChildVC
{
//    HMHomeController *home = [[HMHomeController alloc] init];
    HomeRecommandController *home = [[HomeRecommandController alloc] init];
    MeViewController *me = [[MeViewController alloc] init];
    
    HMNavigationController *navHome = [[HMNavigationController alloc] initWithRootViewController:home];
    HMNavigationController *navMe = [[HMNavigationController alloc] initWithRootViewController:me];
    
    home.tabBarItem.image = [UIImage imageNamed:@"tab_home_un"];
    home.tabBarItem.selectedImage = [self imageWithRenderingMode:@"tab_home_on"];
    
    me.tabBarItem.image = [UIImage imageNamed:@"tab_me"];
    me.tabBarItem.selectedImage = [self imageWithRenderingMode:@"tab_me_on"];
    
    self.tabBar.tintColor = COLOR_Theme;
    
    home.title = @"推荐";
    me.title = @"我的";
    
    [self addChildViewController:navHome];
    [self addChildViewController:navMe];
}

- (UIImage *)imageWithRenderingMode:(NSString *)selectedImage
{
    UIImage *selectedImageName = [UIImage imageNamed:selectedImage];
    return [selectedImageName imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (UIImage *)stretchableImageWithLeftCapWidth:(NSString *)name
{
    UIImage *imageName = [UIImage imageNamed:name];
    return [imageName stretchableImageWithLeftCapWidth:imageName.size.width*0.5 topCapHeight:imageName.size.height*0.5];
}



- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait; // 初始化初始方向
}

@end
