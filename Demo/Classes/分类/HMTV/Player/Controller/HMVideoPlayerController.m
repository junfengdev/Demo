//
//  HMVideoPlayerController.m
//  Demo
//
//  Created by lijunfeng on 16/10/3.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMVideoPlayerController.h"

@interface HMVideoPlayerController ()

@end

@implementation HMVideoPlayerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
}


- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    UIInterfaceOrientation interfaceOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    if (interfaceOrientation == UIDeviceOrientationPortrait || interfaceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        //翻转为竖屏时
       
        
    }else if (interfaceOrientation==UIDeviceOrientationLandscapeLeft || interfaceOrientation == UIDeviceOrientationLandscapeRight) {
        //翻转为横屏时
        self.view.frame = self.view.bounds;
    }
}

@end
