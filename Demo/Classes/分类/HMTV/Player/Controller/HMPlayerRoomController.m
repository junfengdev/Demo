//
//  HMPlayerRoomController.m
//  Demo
//
//  Created by lijunfeng on 16/10/3.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMPlayerRoomController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "HMVideoPlayer.h"
#import "HMPlayerSliderbar.h"
#import "HMPlayerDetail.h"

@interface HMPlayerRoomController ()
@property (nonatomic, strong) HMVideoPlayer *player;
@property (nonatomic, strong) HMPlayerSliderbar *sliderbar;
@property (nonatomic, strong) HMPlayerDetail *playerDetail;

@end

@implementation HMPlayerRoomController

- (instancetype)init{
    if (self == [super init]) {
        self.fd_prefersNavigationBarHidden = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (HMVideoPlayer *)player{
    if (!_player) {
        _player = [[HMVideoPlayer alloc] init];
        _player.backgroundColor = [UIColor redColor];
        [self.view addSubview:_player];
    }
    return _player;
}

- (HMPlayerSliderbar *)sliderbar{
    if (!_sliderbar) {
        _sliderbar = [[HMPlayerSliderbar alloc] init];
        _sliderbar.backgroundColor = [UIColor orangeColor];
        [self.view addSubview:_sliderbar];
    }
    return _sliderbar;
}

- (HMPlayerDetail *)playerDetail{
    if (!_playerDetail) {
        _playerDetail = [[HMPlayerDetail alloc] init];
        _playerDetail.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_playerDetail];
    }
    return _playerDetail;
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];

    UIInterfaceOrientation interfaceOrientation=[[UIApplication sharedApplication] statusBarOrientation];
    if (interfaceOrientation == UIDeviceOrientationPortrait || interfaceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        //翻转为竖屏时
        self.sliderbar.hidden = NO;
        self.playerDetail.hidden = NO;
        
        CGFloat playerW = self.view.width;
        CGFloat playerH = playerW*0.62;
        self.player.frame = CGRectMake(0, 20, playerW, playerH);
        
        CGFloat sliderH = 40;
        self.sliderbar.frame = CGRectMake(0, CGRectGetMaxY(self.player.frame), playerW, sliderH);
        
        CGFloat detailH = self.view.height-20-playerH-sliderH;
        self.playerDetail.frame = CGRectMake(0, CGRectGetMaxY(self.sliderbar.frame), playerW, detailH);
        
        
    }else if (interfaceOrientation==UIDeviceOrientationLandscapeLeft || interfaceOrientation == UIDeviceOrientationLandscapeRight) {
        //翻转为横屏时
        self.sliderbar.hidden = YES;
        self.playerDetail.hidden = YES;
        
        self.player.frame = self.view.bounds;
      
    }
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait; // 初始化初始方向
}

@end
