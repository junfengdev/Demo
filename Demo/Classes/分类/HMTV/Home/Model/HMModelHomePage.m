//
//  HMModelHomePage.m
//  Demo
//
//  Created by lijunfeng on 16/9/11.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMModelHomePage.h"
#import "NSDictionary+NullSafe.h"

@implementation HMModelHomePage

+ (NSMutableArray *)parsingJSonWithData:(NSArray *)data{
    return [[self alloc]parsingJSonWithData:data];
}

- (NSMutableArray *)parsingJSonWithData:(NSArray *)data{
  
    NSMutableArray *arr = [NSMutableArray array];
    
    if (self == [super init]) {
  
        if ([data isKindOfClass:[NSArray class]]) {
            
            if (!IsArrEmpty(data)) {
                
                for (int i = 0 ; i < data.count; i ++) {
                    
                    if ([data[i] isKindOfClass:[NSDictionary class]]) {
                        
                        NSDictionary *dic = data[i];
                        
                        HMModelHomePage *model = [[HMModelHomePage alloc] init];
                        
                        [model setValuesForKeysWithDictionary:dic];
                        [arr addObject:model];
                    }
                }
            }
        }
        
    }
    
    return arr;
}

@end
