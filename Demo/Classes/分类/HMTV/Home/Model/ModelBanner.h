//
//  ModelBanner.h
//  HuoMao
//
//  Created by 李俊峰 on 16/2/3.
//  Copyright © 2016年 woo. All rights reserved.
//

#import "BaseModel.h"

@interface ModelBanner : BaseModel <NSCoding>

@property (nonatomic, copy) NSString * image;
@property (nonatomic, copy) NSString * img_title;
@property (nonatomic, copy) NSString * img_url;     // 如果是纯数字就是跳转房间－http:跳转web
@property (nonatomic, assign) NSInteger screenType;
@property (nonatomic, assign) NSInteger is_channel; // 如果是1就是跳转房间，0就是跳转web
@property (nonatomic, assign) NSInteger type;       // 只有2的时候是户外直播间，此时在判断screenType是否是横竖屏

+ (NSMutableArray *)parseJsonWithDictionary:(NSDictionary *)dict;

@end

/*
{
    "image": "http://static.huomao.com/upload/web/images/advert/20160809210944AVvbhxHu.jpg",
    "img_url": "4398",
    "img_title": "实力语速解说，从不吹比！",
    "type": 0,
    "screenType": 0,
    "is_channel": 1
}*/