//
//  HMModelHomePage.h
//  Demo
//
//  Created by lijunfeng on 16/9/11.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "BaseModel.h"

@interface HMModelHomePage : BaseModel

__string(icon);        //分类图标

__string(gid);         //游戏类型id

__string(cname);       //游戏名称

__mutableArray(data);  //总数据

- (NSMutableArray *)parsingJSonWithData:(NSArray *)data;
+ (NSMutableArray *)parsingJSonWithData:(NSArray *)data;

@end

/*
"gid": "23",
"icon": "http://www.huomao.com/static/api/images/public/rec_icon/DOTA2.png",
"cname": "DOTA2",
"data":@[]
*/
