//
//  ModelBanner.m
//  HuoMao
//
//  Created by 李俊峰 on 16/2/3.
//  Copyright © 2016年 woo. All rights reserved.
//

#import "ModelBanner.h"
#import "NSDictionary+NullSafe.h"

@implementation ModelBanner

/// 模拟服务器给的json
+ (NSDictionary *)testDic{
    NSDictionary *json = @{@"status":@(1),
                           @"data":@[@{@"tag":@""},@{@"img":@"img111111",
                                       @"title":@"图片123"
                                       },@{@"img":@"img22222",
                                           @"title":@"哈哈哈图片"
                                           }
                                     ]
                           };
    return json;
}

+ (NSMutableArray *)parseJsonWithDictionary:(NSDictionary *)dict {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    if ([dict isKindOfClass:[NSDictionary class]]) {
        
        if ([dict objectForKey:@"data"]) {
            
            NSArray *datas = dict[@"data"];
            
            if (!IsArrEmpty(datas)) {
                
                for (int i = 0 ; i < datas.count; i ++) {
                    
                    if ([datas[i] isKindOfClass:[NSDictionary class]]) {
                      
                        NSDictionary *dic = datas[i];
                        
                        ModelBanner *hot = [[ModelBanner alloc] init];
                       
                        [hot setValuesForKeysWithDictionary:dic];
                        
                        [arr addObject:hot];
                    }
                }
            }
        }
    }
    return arr;
}


- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.img_title forKey:@"img_title"];
    [aCoder encodeObject:self.img_url forKey:@"img_url"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.is_channel] forKey:@"is_channel"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.screenType] forKey:@"screenType"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.image = [aDecoder decodeObjectForKey:@"image"];
        self.img_url = [aDecoder decodeObjectForKey:@"img_url"];
        self.img_title = [aDecoder decodeObjectForKey:@"img_title"];
        self.is_channel = [[aDecoder decodeObjectForKey:@"is_channel"] integerValue];
        self.screenType = [[aDecoder decodeObjectForKey:@"screenType"] integerValue];
    }
    return self;
}

@end
