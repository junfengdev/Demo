//
//  HMHomeMainCell.m
//  Demo
//
//  Created by lijunfeng on 2016/12/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMHomeMainCell.h"

@implementation HMHomeMainCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        _thumb = [[UIImageView alloc] init];
        [self addSubview:_thumb];
        
    //            _thumb.layer.cornerRadius = kCornerRadius;
    //            _thumb.layer.shouldRasterize = YES;
    //            _thumb.clipsToBounds = YES;
    //            _thumb.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        UIImage *thumbImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"001" ofType:@"jpg"]];
        _thumb.image = thumbImage;
        
        _shaow = [[UIImageView alloc] init];

        _labNickname = [[UILabel alloc] init];
        _labNickname.textColor = [UIColor whiteColor];
        _labNickname.font = [UIFont systemFontOfSize:11.f];
        _labNickname.textAlignment = NSTextAlignmentLeft;
        
        _btnNums = [[UIButton alloc] init];
        [_btnNums setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnNums.titleLabel.font = [UIFont systemFontOfSize:11.f];
        _btnNums.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        _channel = [[UILabel alloc] init];
        _channel.textColor = [UIColor blackColor];
        _channel.font = [UIFont systemFontOfSize:13.f];
        _channel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_channel];
        
        [_thumb addSubview:_shaow];
        [_thumb addSubview:_labNickname];
        [_thumb addSubview:_btnNums];
        
    }
    return self;
}

- (void)getRecGameWithModel:(NSDictionary *)dic{
    
    NSString *image = dic[@"image"];
    if (image) {
        [self.thumb sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"huomao_default_Heng"]];
    }
    
    NSString *channel = dic[@"channel"];
    if (channel) {
        self.channel.text = [NSString stringWithFormat:@"%@",channel];
    }
    
    NSString *nickname = dic[@"nickname"];
    if (nickname) {
        self.labNickname.text = [NSString stringWithFormat:@"%@",nickname];
    }
    
    NSString *views = dic[@"views"];
    if (views) {
        [self.btnNums setTitle:[NSString stringWithFormat:@"%@",views] forState:UIControlStateNormal];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat channelH = 20;
    self.thumb.frame = CGRectMake(0, 0, self.width, self.height-channelH);
    self.channel.frame = CGRectMake(0, CGRectGetMaxY(self.thumb.frame), self.width, channelH);
    
    self.labNickname.frame = CGRectMake(5, self.thumb.height-20, self.width-10, 20);
    self.btnNums.frame = CGRectMake(self.width-30, self.thumb.height-20, self.width-20, 20);
    
}

@end
