//
//  HMHomeRecTitleView.h
//  Demo
//
//  Created by lijunfeng on 16/9/11.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMModelHomePage.h"

@interface HMHomeRecTitleView : UIView
- (void)getRecGamesDataWithModel:(HMModelHomePage *)model;
@end
