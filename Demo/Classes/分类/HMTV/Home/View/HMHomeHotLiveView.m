//
//  HotLiveView.m
//  HuoMao
//
//  Created by 李俊峰 on 16/1/11.
//  Copyright © 2016年 woo. All rights reserved.
//

#define titleH 35.f

#import "HMHomeHotLiveView.h"
#import "UIImageView+WebCache.h"

@interface HMHomeHotLiveView ()

@property (nonatomic, strong) UIView *titleView;
/**
 *  热门直播标题
 */
@property (nonatomic, strong) UIButton *btnTitle;
@property (nonatomic, strong) UIScrollView *scroll;
@end

@implementation HMHomeHotLiveView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _titleView = [[UIView alloc] initWithFrame:CGRectZero];
        _titleView.frame = CGRectMake(0, 0, frame.size.width, titleH);
        _titleView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_titleView];
        
        _scroll = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scroll.frame = CGRectMake(0, CGRectGetMaxY(self.titleView.frame), frame.size.width, frame.size.height-titleH);
        _scroll.showsHorizontalScrollIndicator = NO;
        [self addSubview:_scroll];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.imgIcon.frame = CGRectMake(5, (titleH-15.0)/2.0, 15, 15);
    self.btnTitle.frame = CGRectMake(CGRectGetMaxX(self.imgIcon.frame)+5, 0, 100, titleH);
}

- (UIButton *)btnTitle{
    if (!_btnTitle) {
        _btnTitle = [[UIButton alloc] initWithFrame:CGRectZero];
        [_btnTitle setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_btnTitle setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnTitle.titleLabel.font = [UIFont systemFontOfSize:SCREEN_WIDTH>600?16.f:14.f];
        [self.titleView addSubview:_btnTitle];
    }
    return _btnTitle;
}

- (UIImageView *)imgIcon{
    if (!_imgIcon) {
        _imgIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.titleView addSubview:_imgIcon];
    }
    return _imgIcon;
}

- (void)getHotLiveData:(NSMutableArray *)hotArray icon:(NSString *)icon{
   
    self.imgIcon.image = [UIImage imageNamed:@"recommand_hotlive"];
    [self.btnTitle setTitle:@"热门直播" forState:UIControlStateNormal];
    
    CGFloat imgW = (SCREEN_WIDTH - kCellSpacing)/2.0;
    CGFloat imgH = imgW*9.0/16;
    
    NSInteger count = hotArray.count;
    for (int i = 0 ; i < count; i ++) {
        if ([hotArray[i] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *modelDic = hotArray[i];
            
            CGFloat imgX = (imgW+kCellSpacing)*i;
            
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(imgX, 0, imgW, imgH)];
            imgView.image = [UIImage imageNamed:@"huomao_default_Heng"];
            imgView.userInteractionEnabled = YES;
            [self.scroll addSubview:imgView];
            
            NSString *imageStr = [NSString stringWithFormat:@"%@",modelDic[@"image"]];
            [imgView sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"huomao_default_Heng"]];
            
            imgView.tag = [[NSString stringWithFormat:@"%@",modelDic[@"cid"]] integerValue];
            
            NSString *channelStr = [NSString stringWithFormat:@"%@",modelDic[@"channel"]];
            UILabel *channel = [[UILabel alloc] initWithFrame:CGRectMake(0, imgH-20, imgW, 20)];
            channel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
            channel.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",channelStr]];
            channel.font = [UIFont systemFontOfSize:SCREEN_WIDTH>600.f?16.f:14.f];
            channel.textColor = [UIColor whiteColor];
            channel.textAlignment = NSTextAlignmentCenter;
            [imgView addSubview:channel];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
            [imgView addGestureRecognizer:tap];
        }
    }
    
    self.scroll.contentSize = CGSizeMake(imgW*count+kCellSpacing*(count-1), 0);
}

- (void)onTap:(UITapGestureRecognizer *)sender{
    if ([self.delegate respondsToSelector:@selector(goLiveRoomWithCid:type:)]) {
        [self.delegate goLiveRoomWithCid:[NSString stringWithFormat:@"%ld",(long)sender.view.tag] type:2];
    }
}
@end
