//
//  HMHomeTableView.h
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HMHomeTableViewDelegate <NSObject>
- (void)goLiveRoomWithCid:(NSString *)cid type:(NSInteger)type;
@end

@interface HMHomeTableView : UITableView

- (void)getHomeScrollData:(NSDictionary *)dic;

- (void)getHomeHotData:(NSDictionary *)dic;

- (void)getHomeRecGamesData:(NSDictionary *)dic;

@property (nonatomic, weak) id <HMHomeTableViewDelegate>tableDelegate;

@property (nonatomic, assign) BOOL isLandscape;

@end
