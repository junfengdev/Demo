//
//  HMHomeMainCell.h
//  Demo
//
//  Created by lijunfeng on 2016/12/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMHomeMainCell : UIView

@property (nonatomic, strong) UIImageView *thumb; // 封面
@property (nonatomic, strong) UIImageView *shaow; // 阴影
@property (nonatomic, strong) UILabel *labNickname; // 昵称
@property (nonatomic, strong) UIButton *btnNums; // 人数
@property (nonatomic, strong) UILabel *channel; // 频道


- (void)getRecGameWithModel:(NSDictionary *)dic;


@end
