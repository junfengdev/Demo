//
//  HMHomeRecTitleView.m
//  Demo
//
//  Created by lijunfeng on 16/9/11.
//  Copyright © 2016年 hm. All rights reserved.
//

/*
 H:50
 line:15
 title:35
 */
#import "HMHomeRecTitleView.h"

@interface HMHomeRecTitleView ()

@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *lineTop;
@property (nonatomic, strong) UIView *lineBottom;

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIImageView *moreImg;
@property (nonatomic, strong) UILabel *moreLab;

@end

@implementation HMHomeRecTitleView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)getRecGamesDataWithModel:(HMModelHomePage *)model{
    self.title.text = [NSString stringWithFormat:@"%@",model.cname];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.icon]] placeholderImage:[UIImage imageNamed:@"recommand_live"]];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat lineH = SCREEN_HEIGHT>600.f?15:10;
    CGFloat titleH = self.height-lineH;

    self.lineView.frame = CGRectMake(0, 0, self.width, lineH);
    self.lineTop.frame = CGRectMake(0, 0, self.width, 0.5);
    self.lineBottom.frame = CGRectMake(0, lineH-1, self.width, 0.5);
    
    self.icon.frame = CGRectMake(10, CGRectGetMaxY(self.lineView.frame)+(titleH-15)/2.0, 15, 15);
    self.title.frame = CGRectMake(CGRectGetMaxX(self.icon.frame)+5, CGRectGetMaxY(self.lineView.frame), 100, titleH);
    
    self.moreLab.frame = CGRectMake(self.width-100, CGRectGetMaxY(self.lineView.frame), 80, titleH);
    self.moreImg.frame = CGRectMake(CGRectGetMaxX(self.moreLab.frame)+5, CGRectGetMaxY(self.lineView.frame)+(titleH-13)/2.0, 13, 13);
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:_lineView];
        _lineView.backgroundColor = COLOR(244, 244, 244);
        
        _lineTop = [[UIView alloc] initWithFrame:CGRectZero];
        [_lineView addSubview:_lineTop];
        _lineTop.backgroundColor = COLOR(220, 220, 220);
        
        _lineBottom = [[UIView alloc] initWithFrame:CGRectZero];
        [_lineView addSubview:_lineBottom];
        _lineBottom.backgroundColor = COLOR(220, 220, 220);
    }
    return _lineView;
}

- (UIImageView *)icon{
    if (!_icon) {
        _icon = [[UIImageView alloc]initWithFrame:CGRectZero];
        _icon.image = [UIImage imageNamed:@"recommand_live"];
        [self addSubview:_icon];
    }
    return _icon;
}

- (UILabel *)title{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectZero];
        _title.text = @"";
        _title.font = [UIFont systemFontOfSize:SCREEN_WIDTH>600.f?16.f:14.f];
        _title.textColor = [UIColor blackColor];
        _title.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_title];
    }
    return _title;
}

- (UILabel *)moreLab{
    if (!_moreLab) {
        _moreLab = [[UILabel alloc] initWithFrame:CGRectZero];
        _moreLab.text = @"更多直播";
        _moreLab.font = [UIFont systemFontOfSize:12];
        _moreLab.textColor = [UIColor lightGrayColor];
        _moreLab.textAlignment = NSTextAlignmentRight;
        [self addSubview:_moreLab];
    }
    return _moreLab;
}

- (UIImageView *)moreImg{
    if (!_moreImg) {
        _moreImg = [[UIImageView alloc]initWithFrame:CGRectZero];
        _moreImg.image = [UIImage imageNamed:@"live_more_lightGray"];
        _moreImg.contentMode =UIViewContentModeScaleAspectFit;
        [self addSubview:_moreImg];
    }
    return _moreImg;
}


@end
