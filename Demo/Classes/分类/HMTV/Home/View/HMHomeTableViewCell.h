//
//  HMHomeTableViewCell.h
//  Demo
//
//  Created by lijunfeng on 2016/12/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMModelHomePage.h"

@interface HMHomeTableViewCell : UITableViewCell
- (void)loadDataWithModel:(HMModelHomePage *)model;
@end
