//
//  HMHomeTableView.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMHomeTableView.h"
#import "HMHomeTableViewCell.h"
#import "SDCycleScrollView.h"
#import "HMHomeHotLiveView.h"
#import "ModelBanner.h"

@interface HMHomeTableView ()<UITableViewDelegate,UITableViewDataSource,SDCycleScrollViewDelegate,HMHomeHotLiveViewDelegate>

@property (nonatomic, strong) NSMutableArray *recAry;
@property (nonatomic, strong) NSMutableArray *hotAry;
/**
 *  轮播图的每个modelBanner
 */
@property (nonatomic, strong) NSMutableArray *bannerArr;
/**
 *  轮播图上面的图片数组
 */
@property (nonatomic,strong)NSMutableArray *bannerImages;
/**
 *  轮播图上面的标题数组
 */
@property (nonatomic, strong) NSMutableArray *bannerTitles;

@end

@implementation HMHomeTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {        
        self.allowsSelection = NO;
        self.delegate = self;
        self.dataSource = self;        
    }
    return self;
}

- (NSMutableArray *)recAry{
    if (!_recAry) {
        _recAry = [NSMutableArray new];
    }
    return _recAry;
}

- (void)getHomeScrollData:(NSDictionary *)dict{
    
    NSMutableArray * bannerArr = [ModelBanner parseJsonWithDictionary:dict];
    
    if (!IsArrEmpty(bannerArr)) {
        
        [self.bannerArr removeAllObjects];
        [self.bannerTitles removeAllObjects];
        [self.bannerImages removeAllObjects];
        
        self.bannerArr = bannerArr;
        
        for (int i = 0 ; i < bannerArr.count ; i++) {
            if ([bannerArr[i] isKindOfClass:[ModelBanner class]]) {
                ModelBanner *banner = bannerArr[i];
                
                if (banner.image && banner.img_title) {
                    [self.bannerImages addObject:banner.image];
                    [self.bannerTitles addObject:banner.img_title];
                }
            }
        }
        
        UITableViewCell *cell = (UITableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        for (UIView *view in cell.contentView.subviews) {
            if ([view isKindOfClass:[SDCycleScrollView class]]) {
                SDCycleScrollView *scroll = (SDCycleScrollView *)view;
                scroll.titlesGroup=self.bannerTitles;
                scroll.imageURLStringsGroup=self.bannerImages;
                [self reloadData];
            }
        }
       
    }
}

- (void)getHomeHotData:(NSDictionary *)dict{
    
    id dataArr = dict[@"data"];
    if ([dataArr isKindOfClass:[NSArray class]]) {
        self.hotAry = dataArr;
        if (!IsArrEmpty(self.hotAry)) {
            NSString *icon = [NSString stringWithFormat:@"%@",dict[@"icon"]];
            
            UITableViewCell *cell = (UITableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            for (UIView *view in cell.contentView.subviews) {
                if ([view isKindOfClass:[HMHomeHotLiveView class]]) {
                    HMHomeHotLiveView *hotView = (HMHomeHotLiveView *)view;
                    [hotView getHotLiveData:self.hotAry icon:icon];
                    [self reloadData];
                }
            }
        }
    }
}

- (void)getHomeRecGamesData:(NSDictionary *)dic{
    if ([dic isKindOfClass:[NSDictionary class]]) {
        if ([dic[@"data"] isKindOfClass:[NSArray class]]) {
            NSMutableArray *arr = dic[@"data"];
            self.recAry = [HMModelHomePage parsingJSonWithData:arr];
            [self reloadData];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return IsArrEmpty(self.recAry)?0:self.recAry.count;
    }
    else{
        return 1;
    }
}

/**
 0: 轮播图
 1: 热门直播
 2: mainCell
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        static NSString *cellID = @"HMHomeTableViewCellOne";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            
            //广告轮播图 720*295
            CGFloat cycH = SCREEN_WIDTH/2.441;
            SDCycleScrollView *_cycleView=[SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, cycH) delegate:self placeholderImage:[UIImage imageNamed:@"huomao_default_Heng"]];
            _cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
            _cycleView.backgroundColor = [UIColor clearColor];
            _cycleView.pageControlAliment=SDCycleScrollViewPageContolAlimentRight;
            _cycleView.currentPageDotColor=COLOR_Theme;
            _cycleView.titleLabelTextColor = [UIColor whiteColor];
            _cycleView.titleLabelHeight = 34;
            _cycleView.titleLabelBackgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
            [cell.contentView addSubview:_cycleView];
            _cycleView.autoScroll = YES;
        }
        
        return cell;
    }
    else if (indexPath.section == 1) {
        static NSString *cellID = @"HMHomeTableViewCellTwo";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            
            //热门直播
            CGFloat hotH = 35.f+((SCREEN_WIDTH - kCellSpacing)/2.0)*(9/16.0);
            HMHomeHotLiveView *_hotView = [[HMHomeHotLiveView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, hotH)];
            _hotView.delegate = self;
            [cell.contentView addSubview:_hotView];
        }

        return cell;
    }
    else{
        static NSString *cellID = @"HMHomeTableViewCellThree";
        
        HMHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[HMHomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        if (!IsArrEmpty(self.recAry)) {
            HMModelHomePage *homePage = self.recAry[indexPath.row];
            [cell loadDataWithModel:homePage];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return SCREEN_WIDTH/2.441;
    }
    else if (indexPath.section == 1){
        return 35.f+((SCREEN_WIDTH - kCellSpacing)/2.0)*(9/16.0);
    }
    else{
        return 50+(((self.width-kCellSpacing)/2.f)*(9/16.f)+25)*2+8;
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (!IsArrEmpty(self.bannerArr)) {
        if ([self.bannerArr[index] isKindOfClass:[ModelBanner class]]) {
            
        }
    }
}

- (NSMutableArray *)bannerArr{
    if (!_bannerArr) {
        _bannerArr = [[NSMutableArray alloc] init];
    }
    return _bannerArr;
}

- (NSMutableArray *)hotAry{
    if (!_hotAry) {
        _hotAry = [[NSMutableArray alloc] init];
    }
    return _hotAry;
}

- (NSMutableArray *)bannerTitles{
    if (!_bannerTitles) {
        _bannerTitles = [[NSMutableArray alloc] init];
    }
    return _bannerTitles;
}

- (NSMutableArray *)bannerImages{
    if (!_bannerImages) {
        _bannerImages = [[NSMutableArray alloc]init];
    }
    return _bannerImages;
}

@end
