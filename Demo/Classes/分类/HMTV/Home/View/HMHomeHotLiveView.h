//
//  HotLiveView.h
//  HuoMao
//
//  Created by 李俊峰 on 16/1/11.
//  Copyright © 2016年 woo. All rights reserved.
//  首页热门直播

#import <UIKit/UIKit.h>

@class HMHomeHotLiveView;

@protocol HMHomeHotLiveViewDelegate <NSObject>
/**
 *  热门直播点击跳转直播间的代理方法
 *
 *  @param view    HMHomeHotLiveView
 *  @param hotlive HMModelHomeRecommend
 */
- (void)goLiveRoomWithCid:(NSString *)cid type:(NSInteger)type;

@end

@interface HMHomeHotLiveView : UIView
/**
 *  热门直播icon
 */
@property (nonatomic, strong) UIImageView *imgIcon;

@property (nonatomic, weak) id <HMHomeHotLiveViewDelegate>delegate;

- (void)getHotLiveData:(NSMutableArray *)hotArray icon:(NSString *)icon;

@end
