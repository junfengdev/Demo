//
//  HMHomeTableViewCell.m
//  Demo
//
//  Created by lijunfeng on 2016/12/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMHomeTableViewCell.h"
#import "HMHomeRecTitleView.h"
#import "HMHomeMainCell.h"

@interface HMHomeTableViewCell ()
@property (nonatomic, strong)HMHomeRecTitleView *titleView;
@end

@implementation HMHomeTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
     
        HMHomeRecTitleView *title = [[HMHomeRecTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
        [self addSubview:title];
        self.titleView = title;
        
        CGFloat width = (SCREEN_WIDTH - kCellSpacing)/2.0;
        CGFloat height = width*9.0/16 + 25 ;
        
        for (int i = 0; i < 4; i++) {
            int X = i%2;
            int Y = i/2;
            
            CGFloat cvX = X*(width+kCellSpacing);
            CGFloat cvY = Y*(height + kCellSpacing)+50;
            
            HMHomeMainCell *cell = [[HMHomeMainCell alloc]initWithFrame:CGRectMake(cvX, cvY, width, height)];
            cell.tag = i;

            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
            [cell addGestureRecognizer:tap];
            
            [self.contentView addSubview:cell];
        }
    }
    return self;
}

// 展会数据
- (void)loadDataWithModel:(HMModelHomePage *)model
{
    [self.titleView getRecGamesDataWithModel:model];
    
    for (int i=0; i< self.contentView.subviews.count; i++) {
        if ([self.contentView.subviews[i] isKindOfClass:[HMHomeMainCell class]]) {
            HMHomeMainCell *clickView = self.contentView.subviews[i];
            if (model.data.count <= 4) { // 4个
                [clickView getRecGameWithModel:model.data[i]];
            }
        }
    }
}

// 点击事件的处理
- (void)tapAction:(UITapGestureRecognizer *)tapView {
    
    
}

@end
