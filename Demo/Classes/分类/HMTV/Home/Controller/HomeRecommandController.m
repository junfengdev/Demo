//
//  HomeRecommandController.m
//  Demo
//
//  Created by lijunfeng on 16/11/8.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HomeRecommandController.h"
#import "HMHomeTableView.h"
#import "HMPlayerRoomController.h"
#import <MJRefresh/MJRefresh.h>


@interface HomeRecommandController ()<HMHomeTableViewDelegate,SDCycleScrollViewDelegate,HMHomeHotLiveViewDelegate>
@property (nonatomic, strong) HMHomeTableView *tableView;

@end

@implementation HomeRecommandController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _tableView = [[HMHomeTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-44-49-60) style:UITableViewStylePlain];
        _tableView.tableDelegate = self;
        [self.view addSubview:_tableView];
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat cycH = SCREEN_WIDTH/2.6;
        CGFloat hotH = 35.f+((SCREEN_WIDTH - kCellSpacing)/2.0)*(9/16.0);
        CGFloat headH = cycH+hotH;
        
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, headH)];
        _headerView.backgroundColor = [UIColor redColor];
        self.tableView.tableHeaderView = _headerView;
        
        //广告轮播图
        _cycleView=[SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, _headerView.width, cycH) delegate:self placeholderImage:nil];
        _cycleView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
        _cycleView.backgroundColor = [UIColor clearColor];
        _cycleView.pageControlAliment=SDCycleScrollViewPageContolAlimentRight;
        _cycleView.currentPageDotColor=COLOR_Theme;
        _cycleView.titleLabelTextColor = [UIColor whiteColor];
        _cycleView.titleLabelHeight = 34;
        _cycleView.titleLabelBackgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
        [_headerView addSubview:_cycleView];
        _cycleView.autoScroll = YES;
        
        //热门直播
        _hotView = [[HMHomeHotLiveView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_cycleView.frame), _headerView.width, hotH)];
        _hotView.delegate = self;
        [_headerView addSubview:_hotView];
        
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(onClickDismissTabController)];
    
    _tableView = [[HMHomeTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.tableDelegate = self;
    [self.view addSubview:_tableView];
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
   
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf prepareData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)prepareData {
    [self getBannerData];
    [self getHotLiveData];
    [self getRecGamesData];
    [self.tableView.mj_header endRefreshing];
}

- (void)getBannerData{
    [HMHttpManager getBannerImages:^(NSDictionary *dict) {
        if ([dict isKindOfClass:[NSDictionary class]]) {
            
            if ([dict[@"status"] intValue] == 1) {
                [self.tableView getHomeScrollData:dict];

            }
        }
    } error:^(NSError *errorBack) {
        
    }];
}

- (void)getHotLiveData{
    [HMHttpManager getHomeRecommend:^(NSDictionary *dict) {
        if ([dict isKindOfClass:[NSDictionary class]]) {
            if ([dict[@"status"] intValue] == 1) {
                [self.tableView getHomeHotData:dict];
            }
        }
    } error:^(NSError *error) {
        
    }];
}

- (void)getRecGamesData{
    [HMHttpManager getHomeRecGames:^(NSDictionary *dict) {
        if ([dict isKindOfClass:[NSDictionary class]]) {
            if ([dict[@"status"] intValue] == 1) {
                [self.tableView getHomeRecGamesData:dict];
            }
        }
    } error:^(NSError *error) {
        
    }];
}



- (void)goLiveRoomWithCid:(NSString *)cid type:(NSInteger)type{
    switch (type) {
        case 1:
        {
            
        }
            break;
        case 2: // 热门直播
        {
            
        }
            break;
        default:
            break;
    }
}




- (void)onClickDismissTabController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
