//
//  HMReadyLiveViewController.m
//  Demo
//
//  Created by admin on 16/9/21.
//  Copyright © 2016年 hm. All rights reserved.
//
#define liveBtnW 60
#define liveBtnH 80

#import "HMReadyLiveViewController.h"
#import "HMLiveShowViewController.h"

@interface HMReadyLiveViewController ()
{
    UIImageView *_bg;
}
@property (nonatomic, strong) UIButton  *closeBtn;
@property (nonatomic, strong) UIButton  *liveLandScape;
@property (nonatomic, strong) UIButton  *livePortrait;

@property (nonatomic, strong) UIVisualEffectView *visual;
@end

@implementation HMReadyLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _bg.image = [UIImage imageNamed:@"2welcome1136"];
    [self.view addSubview:_bg];
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    _visual = [[UIVisualEffectView alloc] initWithEffect:blur];
    _visual.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [_bg addSubview:_visual];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showLiveBtnAnimated];
}

- (UIButton *)closeBtn{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setTitle:@"X" forState:UIControlStateNormal];
        [_closeBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(onClickButton) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_closeBtn];
    }
    return _closeBtn;
}

- (UIButton *)liveLandScape{
    if (!_liveLandScape) {
        _liveLandScape = [UIButton buttonWithType:UIButtonTypeCustom];
        [_liveLandScape setTitle:@"横屏开播" forState:UIControlStateNormal];
        [_liveLandScape setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [_liveLandScape addTarget:self action:@selector(onClickButtonLive:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_liveLandScape];
        _liveLandScape.titleLabel.adjustsFontSizeToFitWidth = YES;
        _livePortrait.tag = 2;
    }
    return _liveLandScape;
}

- (UIButton *)livePortrait{
    if (!_livePortrait) {
        _livePortrait = [UIButton buttonWithType:UIButtonTypeCustom];
        [_livePortrait setTitle:@"竖屏开播" forState:UIControlStateNormal];
        [_livePortrait setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [_livePortrait addTarget:self action:@selector(onClickButtonLive:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_livePortrait];
        _livePortrait.titleLabel.adjustsFontSizeToFitWidth = YES;
        _livePortrait.tag = 1;
    }
    return _livePortrait;
}

- (void)onClickButtonLive:(UIButton *)sender{
    HMLiveShowViewController *show = [[HMLiveShowViewController alloc]init];
    show.isLandScape = sender.tag==1?NO:YES;
    [self presentViewController:show animated:YES completion:nil];
}

- (void)showLiveBtnAnimated{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.liveLandScape.frame = CGRectMake((self.view.width-liveBtnW)/4.f, (self.view.height-liveBtnH-100), liveBtnW, liveBtnH);
        self.livePortrait.frame = CGRectMake((self.view.width-liveBtnW)/4.f*3, (self.view.height-liveBtnH-100), liveBtnW, liveBtnH);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.1 animations:^{
            self.liveLandScape.frame = CGRectMake((self.view.width-liveBtnW)/4.f, (self.view.height-liveBtnH-80), liveBtnW, liveBtnH);
            self.livePortrait.frame = CGRectMake((self.view.width-liveBtnW)/4.f*3, (self.view.height-liveBtnH-80), liveBtnW, liveBtnH);
        }];
    }];
}

- (void)onClickButton{
    [UIView animateWithDuration:0.1 animations:^{
        self.liveLandScape.frame = CGRectMake((self.view.width-liveBtnW)/4.f, (self.view.height), liveBtnW, liveBtnH);
        self.livePortrait.frame = CGRectMake((self.view.width-liveBtnW)/4.f*3, (self.view.height), liveBtnW, liveBtnH);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    _bg.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    _visual.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    self.closeBtn.frame = CGRectMake((self.view.width-40)/2, (self.view.height-40-20), 40, 40);
    
    self.liveLandScape.frame = CGRectMake((self.view.width-liveBtnW)/4.f, (self.view.height), liveBtnW, liveBtnH);
    self.livePortrait.frame = CGRectMake((self.view.width-liveBtnW)/4.f*3, (self.view.height), liveBtnW, liveBtnH);
}

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
