//
//  HMHomeController.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMHomeController.h"
#import "HomeRecommandController.h"

@interface HMHomeController ()<WMPageControllerDelegate,
WMPageControllerDataSource>

@property (nonatomic, strong) NSMutableArray *titleArr;
@end

@implementation HMHomeController

- (void)setupDefualt{
    self.menuViewStyle = WMMenuViewStyleLine;
    self.progressColor = COLOR_Theme;
    self.bounces = YES;
    self.itemMargin = 20;
}

- (NSMutableArray *)titleArr{
    if (!_titleArr) {
        _titleArr = [[NSMutableArray alloc] init];
    }
    return _titleArr;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupDefualt];
    }
    return self;
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    if (IsArrEmpty(self.titleArr)) {
        return 1;
    }else{
        return 1+self.titleArr.count;
    }
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index{
    if (index == 0) {
        HomeRecommandController *rec = [[HomeRecommandController alloc] init];
        return rec;
    }
    else
    {
        return nil;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index
{
    if (index == 0) {
        return @"推荐";
    }
    return @"";
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(onClickDismissTabController)];
}


- (void)onClickDismissTabController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
