//
//  HMLiveShowViewController.m
//  Demo
//
//  Created by admin on 16/9/21.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMLiveShowViewController.h"
#import "LiveShowView.h"

@interface HMLiveShowViewController ()

@end

@implementation HMLiveShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:[[LiveShowView alloc] initWithFrame:self.view.bounds]];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if(self.isLandScape){
        return UIInterfaceOrientationMaskLandscape;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
