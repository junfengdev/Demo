//
//  MeViewController.m
//  Demo
//
//  Created by lijunfeng on 16/9/10.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "MeViewController.h"

@interface MeViewController ()
{
    UIImageView *_bg;
}
@end

@implementation MeViewController

- (void)loadView{
    [super loadView];
    
    _bg = [[UIImageView alloc] init];
    _bg.image = [UIImage imageNamed:@"me_background2"];
    _bg.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_bg];
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    _bg.frame = self.view.bounds;
    
}

@end
