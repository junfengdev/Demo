//
//  AdvertiseViewController.h
//  Demo
//
//  Created by lijunfeng on 16/9/4.
//  Copyright © 2016年 hm. All rights reserved.
//

/*
如果是想把整个应用程序的状态栏都隐藏掉，操作如下：
在info.plist上添加一项：Status bar is initially hidden，value为YES；
 */

#import <UIKit/UIKit.h>

@interface AdvertiseViewController : UIViewController

@end
