//
//  AdvertiseView.m
//  Demo
//
//  Created by lijunfeng on 16/9/4.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "AdvertiseView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DACircularProgressView.h"

@interface AdvertiseView()

@property (nonatomic, strong) UIImageView *adView;

@property (nonatomic, strong) UIButton *countBtn;

@property (nonatomic, strong) NSTimer *countTimer;

@property (nonatomic, assign) int count;

@property (nonatomic, strong) DACircularProgressView *progressView;

/**
 *  播放本地视频
 */
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

@end

// 广告显示的时间
static int const showtime = 3;
AdLaunchType launchType = AdLaunchTimerType;

@implementation AdvertiseView

- (NSTimer *)countTimer
{
    if (!_countTimer) {
        _countTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    }
    return _countTimer;
}

- (instancetype)initWithFrame:(CGRect)frame launchType:(AdLaunchType)type
{
    if (self = [super initWithFrame:frame]) {
       
        launchType = type;
        
        [self adView];
        
        [self showProgressView];
        
       // [self moviePlayer];
        
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.adView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    
    CGFloat btnW = 60;
    CGFloat btnH = 30;
    self.countBtn.frame = CGRectMake(kScreenWidth - btnW - 10, 20, btnW, btnH);
    self.progressView.frame =  CGRectMake(kScreenWidth-60, 20, 40, 40);
    
}
- (UIImageView *)adView{
    if (!_adView) {
        
        _adView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _adView.userInteractionEnabled = YES;
        _adView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _adView.clipsToBounds = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushToAd)];
        [_adView addGestureRecognizer:tap];
        [self addSubview:_adView];
    }
    return _adView;
}

- (void)showProgressView{
    
    if (launchType == AdLaunchTimerType) {
        CGFloat btnW = 60;
        CGFloat btnH = 30;
        if (!_countBtn) {
            _countBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - btnW - 10, 20, btnW, btnH)];
            [_countBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
            [_countBtn setTitle:[NSString stringWithFormat:@"%d 跳过",showtime] forState:UIControlStateNormal];
            _countBtn.titleLabel.font = [UIFont systemFontOfSize:13];
            [_countBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            _countBtn.backgroundColor = [UIColor colorWithRed:38 /255.0 green:38 /255.0 blue:38 /255.0 alpha:0.5];
            _countBtn.layer.cornerRadius = 15.f;
            [self addSubview:_countBtn];
        }
    }
    
    if (launchType == AdLaunchProgressType) {
        if (!_progressView) {
            _progressView = [[DACircularProgressView alloc] initWithFrame: CGRectMake(kScreenWidth-60, 20, 40, 40)];
            _progressView.userInteractionEnabled = NO;
            _progressView.progress = 0;
            _progressView.trackTintColor = [UIColor whiteColor];
            _progressView.progressTintColor = [UIColor redColor];
            [self addSubview:_progressView];
            [_progressView setProgress: 1 animated: YES initialDelay: 0 withDuration:showtime];
            
            UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(3, 3, 34, 34)];
            [btn setTitle: @"跳" forState: UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:15.f];
            btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            btn.backgroundColor = [UIColor blackColor];
            [btn addTarget:self action:@selector(dismiss) forControlEvents: UIControlEventTouchUpInside];
            [_progressView addSubview: btn];
            btn.layer.cornerRadius = 17;
            btn.clipsToBounds = YES;
        }
       
    }
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    _adView.image = [UIImage imageWithContentsOfFile:filePath];
}

- (void)pushToAd{
    
    [self dismiss];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushtoad" object:nil userInfo:nil];
}

- (void)countDown
{
    _count --;
    [_countBtn setTitle:[NSString stringWithFormat:@"%d 跳过",_count] forState:UIControlStateNormal];
    if (_count == 0) {
        [self.countTimer invalidate];
        self.countTimer = nil;
        [self dismiss];
    }
}

- (void)show
{
    // 倒计时方法1：GCD
    //    [self startCoundown];
    
    // 倒计时方法2：定时器
    [self startTimer];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self];
}

// 定时器倒计时
- (void)startTimer
{
    _count = showtime;
    [[NSRunLoop mainRunLoop] addTimer:self.countTimer forMode:NSRunLoopCommonModes];
}

// GCD倒计时
- (void)startCoundown
{
    __block int timeout = showtime + 1; //倒计时时间 + 1
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self dismiss];
                
            });
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_countBtn setTitle:[NSString stringWithFormat:@"%d 跳过",timeout] forState:UIControlStateNormal];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

// 移除广告页面
- (void)dismiss
{
    if (_countBtn != nil) {
       [self.countBtn removeFromSuperview];
    }
    if (_progressView != nil) {
        [self.progressView removeFromSuperview];
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.transform=CGAffineTransformMakeScale(1.2, 1.2);
        self.alpha = 0;
                
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


#pragma mark - 播放视频
- (MPMoviePlayerController *)moviePlayer{
    if (!_moviePlayer) {
        
        // 3、播放视频
        NSString *urlStr = [[NSBundle mainBundle] pathForResource:@"keep" ofType:@"mp4"];
        
        if (urlStr) {
            NSURL *url = [NSURL fileURLWithPath:urlStr];
            
            _moviePlayer = [[MPMoviePlayerController alloc]initWithContentURL:url];
            
            [_moviePlayer play];
            [_moviePlayer.view setFrame:self.bounds];
            
            [self addSubview:_moviePlayer.view];
            
            _moviePlayer.shouldAutoplay = YES;
            [_moviePlayer setControlStyle:MPMovieControlStyleNone];
            [_moviePlayer setFullscreen:YES];
            
            [_moviePlayer setRepeatMode:MPMovieRepeatModeOne];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playbackStateChanged)
                                                         name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                       object:_moviePlayer];

        }
        
    }
    return _moviePlayer;
}

- (void)playbackStateChanged{
    //取得目前状态
    MPMoviePlaybackState playbackState = [_moviePlayer playbackState];
    
    //状态类型
    switch (playbackState) {
            case MPMoviePlaybackStateStopped:
            [_moviePlayer play];
            break;
            
            case MPMoviePlaybackStatePlaying:
            NSLog(@"播放中");
            break;
            
            case MPMoviePlaybackStatePaused:
            [_moviePlayer play];
            break;
            
            case MPMoviePlaybackStateInterrupted:
            NSLog(@"播放被中断");
            break;
            
            case MPMoviePlaybackStateSeekingForward:
            NSLog(@"往前快转");
            break;
            
            case MPMoviePlaybackStateSeekingBackward:
            NSLog(@"往后快转");
            break;
            
        default:
            NSLog(@"无法辨识的状态");
            break;
    }
}

- (void)dealloc{
    NSLog(@"%s",__func__);
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                  object:_moviePlayer];
}

@end
