//
//  AdvertiseView.h
//  Demo
//
//  Created by lijunfeng on 16/9/4.
//  Copyright © 2016年 hm. All rights reserved.
//

/*
 广告页面逻辑：
 1、每次进入APP的时候都要先判断沙盒下面是否有广告图片，如果有，就直接显示
 2、无论沙盒下面是否存在广告图片，都要重新调用广告接口，判断广告是否更新；如果更新广告的话就下载新的广告然后删除旧的广告！
 */

#define kScreenWidth  [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

typedef NS_ENUM(NSUInteger, AdLaunchType) {
    AdLaunchProgressType = 0,
    AdLaunchTimerType
};

#import <UIKit/UIKit.h>

@interface AdvertiseView : UIView

- (instancetype)initWithFrame:(CGRect)frame launchType:(AdLaunchType)type;

/** 显示广告页面方法*/
- (void)show;

/** 图片路径*/
@property (nonatomic, copy) NSString *filePath;


@end
