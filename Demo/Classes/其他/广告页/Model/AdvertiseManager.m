//
//  AdvertiseManager.m
//  Demo
//
//  Created by lijunfeng on 16/9/4.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "AdvertiseManager.h"

@implementation AdvertiseManager

/**
 *  判断文件是否存在
 */
+ (BOOL)isFileExistWithFilePath:(NSString *)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = FALSE;
    return [fileManager fileExistsAtPath:filePath isDirectory:&isDirectory];
}

/**
 *  下载新图片
 */
+ (void)downloadAdImageWithUrl:(NSString *)imageUrl imageName:(NSString *)imageName
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        UIImage *image = [UIImage imageWithData:data];
        
        NSString *filePath = [self getFilePathWithImageName:imageName]; // 保存文件的名称
        
        if ([UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES]) {// 保存成功
            NSLog(@"保存成功");
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self deleteOldImage];
                
                [kUserDefaults setObject:imageName forKey:adImageName];
                [kUserDefaults synchronize];
            });
            
        }else{
            NSLog(@"保存失败");
        }
    });
}

+ (void)deleteOldImage{
    NSString *oldImageName = [kUserDefaults valueForKey:adImageName];
    
    if (oldImageName) {
        NSString *oldFilePath = [self getFilePathWithImageName:oldImageName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager removeItemAtPath:oldFilePath error:nil]) {
            NSLog(@"删除图片成功");
        }
    }
}

/**
 *  根据图片名拼接文件路径
 */
+ (NSString *)getFilePathWithImageName:(NSString *)imageName
{
    if (imageName) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:imageName];
        return filePath;
    }
    return nil;
}



@end
