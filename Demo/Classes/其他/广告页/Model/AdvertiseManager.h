//
//  AdvertiseManager.h
//  Demo
//
//  Created by lijunfeng on 16/9/4.
//  Copyright © 2016年 hm. All rights reserved.
//

#define kUserDefaults [NSUserDefaults standardUserDefaults]
static NSString *const adImageName = @"adImageName";
static NSString *const adUrl = @"adUrl";

#import <UIKit/UIKit.h>

@interface AdvertiseManager : NSObject
/**
 *  判断路径是否是文件
 *
 *  @param filePath 路径
 *
 */
+ (BOOL)isFileExistWithFilePath:(NSString *)filePath;

/**
 *  下载广告页面
 *
 *  @param imageUrl  广告URL
 *  @param imageName 广告名称
 */
+ (void)downloadAdImageWithUrl:(NSString *)imageUrl imageName:(NSString *)imageName;

/**
 *  根据名称获取广告路径
 *
 *  @param imageName 广告名
 *
 */
+ (NSString *)getFilePathWithImageName:(NSString *)imageName;

@end
