//
//  InterViewQuestionsController+NSString.h
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "InterViewQuestionsController.h"

@interface InterViewQuestionsController (NSString)
- (void)testNSStringLength;
@end
