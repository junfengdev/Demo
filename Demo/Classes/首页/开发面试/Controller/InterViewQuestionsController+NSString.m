//
//  InterViewQuestionsController+NSString.m
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "InterViewQuestionsController+NSString.h"

@implementation InterViewQuestionsController (NSString)

#pragma mark - 1、计算字符串的长度
/**
 * 汉字2个字节
 * 英文1个字节
 * 符号1个字节
 */
- (void)testNSStringLength{
    
    // 1、纯数字，纯字母，纯汉字
    NSString *pureNum = @"1234567890";
    NSString *pureLetter = @"abcdefg";
    NSString *pureChinese = @"我在测试字符串的长度";
    NSLog(@"pureNum===%lu,pureLetter===%lu,pureChinese===%lu",(unsigned long)pureNum.length,(unsigned long)pureLetter.length,(unsigned long)pureChinese.length);
    
    // 2、中英文混编，符号(中英文状态下的)
    NSString *mixedString = @"我在测试字符串的长度abcdefg";
    NSLog(@"mixedString===%lu",(unsigned long)mixedString.length);
    
    // 3、混编
    NSLog(@"转化的:%d",[self textLength:mixedString]);
}

/// 计算字符串长度
- (int)textLength:(NSString *)text
{
    if(text == nil) return 0;
    
    CGFloat number = 0.0;
    for (int index = 0; index < [text length]; index++)
    {
        NSString *character = [text substringWithRange:NSMakeRange(index, 1)];
        
        if ([character lengthOfBytesUsingEncoding:NSUTF8StringEncoding] == 3)
        {
            number++;
        }
        else
        {
            number = number + 0.5;
        }
    }
    return ceil(number);
}

/// 判断是否是纯数字
- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

/// 判断是否为浮点形
- (BOOL)isPureFloat:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

/// 是否是纯汉字
- (BOOL)isChinese
{
    NSString *match = @"(^[\u4e00-\u9fa5]+$)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:self];
}

/// 是否包含汉字
- (BOOL)includeChinese:(NSString *)string
{
    for(int i=0; i< [string length];i++)
    {
        int a =[string characterAtIndex:i];
        if( a >0x4e00&& a <0x9fff){
            return YES;
        }
    }
    return NO;
}


@end
