//
//  InterViewQuestionsController.m
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "InterViewQuestionsController.h"
#import "InterViewQuestionsController+NSString.h"
#import "InterViewQuestionsController+StatementJudge.h"

@interface InterViewQuestionsController ()

@end

@implementation InterViewQuestionsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testNSStringLength];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self testStatementJudge];
}

@end
