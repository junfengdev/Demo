//
//  InterViewQuestionsController+StatementJudge.m
//  Demo
//
//  Created by lijunfeng on 16/11/24.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "InterViewQuestionsController+StatementJudge.h"

@implementation InterViewQuestionsController (StatementJudge)

- (void)testStatementJudge{
    
    [self testIf:@"guess"];
    
    [self testSwitch:@"gues"];
}

- (void)testIf:(NSString *)type{
    NSTimeInterval start = CACurrentMediaTime();
    if ([type isEqual:@"add"]) {
        NSLog(@"add");
    }
    if ([type isEqual:@"bean"]) {
        NSLog(@"bean");
    }
    if ([type isEqual:@"gift"]) {
        NSLog(@"gift");
    }
    if ([type isEqual:@"guess"]) {
        NSLog(@"guess");
    }
    if ([type isEqual:@"fg"]) {
        NSLog(@"fg");
    }
    if ([type isEqual:@"abc"]) {
        NSLog(@"abc");
    }
    NSLog(@"时长 if == %f",CACurrentMediaTime()-start);
}

- (void)testSwitch:(NSString *)type{
    NSTimeInterval start = CACurrentMediaTime();
    
    NSUInteger index = [self index:type];
    switch (index) {
        case 0: // add
            NSLog(@"add");
            break;
        case 1: // bean
            NSLog(@"bean");
            break;
        case 2: // gift
            NSLog(@"gift");
            break;
        case 3: // guess
            NSLog(@"guess");
            break;
        case 4: // fg
            NSLog(@"fg");
            break;
        case 5: // abc
            NSLog(@"abc");
            break;
        default:
            break;
    }
    NSLog(@"时长 switch == %f",CACurrentMediaTime()-start);
}

- (NSUInteger)index:(NSString *)type{
    NSArray *arr = @[@"add",@"bean",@"gift",@"guess",@"fg",@"abc"];
    if ([arr containsObject:type]) {
        return [arr indexOfObject:type];
    }
    return 999999;
}

@end
