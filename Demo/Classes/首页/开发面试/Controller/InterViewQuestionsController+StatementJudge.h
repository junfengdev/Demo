//
//  InterViewQuestionsController+StatementJudge.h
//  Demo
//
//  Created by lijunfeng on 16/11/24.
//  Copyright © 2016年 hm. All rights reserved.
//  条件语句判断

#import "InterViewQuestionsController.h"

@interface InterViewQuestionsController (StatementJudge)

- (void)testStatementJudge;

@end
