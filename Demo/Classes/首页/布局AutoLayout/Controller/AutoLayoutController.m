//
//  AutoLayoutController.m
//  Demo
//
//  Created by admin on 16/9/12.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "AutoLayoutController.h"

@interface AutoLayoutController ()
{
    NSTimer *_timer;
    TestView *_largeView;
    TestView *_smallView;
}
@end

@implementation AutoLayoutController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self test_2];
}


#pragma mark - 1、init初始化不会触发layoutSubviews [正确的]
- (void)test_1{
    /*
     解释:
     走了initWithFrame:方法,但是又有frame值为{{0, 0}, {0, 0}},并不需要绘制任何的东西,
     所以即使添加了test,也没必要绘制它,同时也验证了addSubview会触发layoutSubviews是错
     误的,只有当被添加的view有着尺寸的时候才会触发layoutSubviews
     */
    
    TestView *test = [[TestView alloc]init];
    [self.view addSubview:test];
}

#pragma mark - 2、addSubview会触发layoutSubviews [不完全正确,当frame为0时是不会触发的]
- (void)test_2{
    TestView *test = [[TestView alloc]init];
    test.frame = CGRectMake(0, 0, 100, 100);
    [self.view addSubview:test];
}

#pragma mark - 3、设置view的Frame会触发layoutSubviews，当然前提是frame的值设置前后发生了变化 [正确]
- (void)test_3{
    /*
     解释:
     layoutSubviews这个方法自身无法调用,是被父类添加的时候才执行的方法
     */
    TestView *test = [[TestView alloc]init];
    test.frame = CGRectMake(0, 0, 100, 100);
    UIView *showView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [test addSubview:showView];
    
    //一个view是不能够自己调用layoutSubviews,如果要调用,需要调用 setNeedsLayout或者 layoutIfNeeded
//    [test setNeedsLayout];
//    [test layoutIfNeeded];
}

#pragma mark - 5、改变一个UIView大小的时候也会触发父UIView上的layoutSubviews事件
- (void)test_5
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.f
                                              target:self
                                            selector:@selector(timerEvent:)
                                            userInfo:nil
                                             repeats:YES];
    _largeView = [[TestView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_largeView];
    
    _smallView = [[TestView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [_largeView addSubview:_smallView];
}

- (void)timerEvent:(id)sender
{
    _smallView.frame = CGRectMake(arc4random()%100 + 20,
                                  arc4random()%100 + 20,
                                  arc4random()%100 + 20,
                                  arc4random()%100 + 20);
    NSLog(@"_smallView %@", _smallView);
    NSLog(@"_largeView %@", _largeView);
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    NSLog(@"%s,width:%f,height:%f",__func__,self.view.width,self.view.height);
}

@end


@interface TestView ()

@end

@implementation TestView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        NSLog(@"%s,initWithFrame:%@",__func__,NSStringFromCGRect(self.frame));
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    NSLog(@"layoutSubviews %@", self);
}

@end
