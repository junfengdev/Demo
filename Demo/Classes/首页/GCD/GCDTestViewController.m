//
//  GCDTestViewController.m
//  Demo
//
//  Created by lijunfeng on 16/12/27.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "GCDTestViewController.h"
#import "MsgdownloadManager.h"

@interface GCDTestViewController ()

@property (nonatomic, strong) NSMutableArray *taskArr;

@end

@implementation GCDTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(onTimerHandler) userInfo:nil repeats:YES];
}

- (NSMutableArray *)taskArr{
    if (!_taskArr) {
        _taskArr = [[NSMutableArray alloc] init];
    }
    return _taskArr;
}

- (void)onTimerHandler{

    NSLog(@"1、taskArr==>%ld",self.taskArr.count);

    [MsgdownloadManager insertObject:@"1"];
    
    [self test];
}

-(void)test{

    dispatch_queue_t q = dispatch_queue_create("fs", DISPATCH_QUEUE_SERIAL);

    NSLog(@"2、taskArr==>%ld",self.taskArr.count);

    if (self.taskArr.count > 0) {
        for (int i=0; i<self.taskArr.count; i++) {
            dispatch_async(q, ^{
                id task = self.taskArr[i];
                [UIView animateWithDuration:1 animations:^{
                    [self.taskArr removeObject:task];
                    NSLog(@"任务==>taskArr==>%ld",self.taskArr.count);
                }];
            });
        }
    }
    
}


@end
