//
//  MsgdownloadManager.h
//  Demo
//
//  Created by lijunfeng on 16/12/27.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MsgdownloadManager : NSObject

+ (void)insertObject:(id)object;

+ (void)removeObject:(id)object;

@end
