//
//  TestSuperViewController.m
//  Demo
//
//  Created by admin on 16/9/26.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "TestSuperViewController.h"
#import "Father.h"
#import "Son.h"

@interface TestSuperViewController ()

@end

@implementation TestSuperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    
}

/*
 2016-09-26 16:56:28.771 Demo[13711:312540] -[Father test] Father,NSObject,Father
 
 2016-09-26 16:56:28.772 Demo[13711:312540] -[Father test] Son,Father,Son --->这个是son类中[super test]方法调用
 2016-09-26 16:56:28.772 Demo[13711:312540] -[Son test] Son,Father,Son
 */
- (void)testFatherAndSon{
    Father *father = [[Father alloc] init];
    [father test];
    
    Son *son = [[Son alloc] init];
    [son test];
}

/*
 2016-09-26 16:56:28.771 Demo[13711:312540] -[Father test] Father,NSObject,Father
 */
- (void)testFather{
    Father *father = [[Father alloc] init];
    [father test];
}

/*
 2016-09-26 16:56:28.772 Demo[13711:312540] -[Son test] Son,Father,Son
 */
- (void)testSon{
    Son *son = [[Son alloc] init];
    [son test];
}


@end
