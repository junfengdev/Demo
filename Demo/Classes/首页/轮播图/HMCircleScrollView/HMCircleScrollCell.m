//
//  HMCircleScrollCell.m
//  Demo
//
//  Created by lijunfeng on 2016/10/22.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "HMCircleScrollCell.h"

@implementation HMCircleScrollCell{
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}

- (UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_label];
    }
    return _label;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.imageView.frame = self.contentView.bounds;
    self.label.frame = CGRectMake(0, 0, self.contentView.width, 30);
}

@end
