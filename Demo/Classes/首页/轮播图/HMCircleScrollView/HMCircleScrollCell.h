//
//  HMCircleScrollCell.h
//  Demo
//
//  Created by lijunfeng on 2016/10/22.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMCircleScrollCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *label;
@end
