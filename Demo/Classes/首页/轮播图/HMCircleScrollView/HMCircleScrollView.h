//
//  HMCircleScrollView.h
//  Demo
//
//  Created by lijunfeng on 2016/10/21.
//  Copyright © 2016年 hm. All rights reserved.
//

/**
 * 1、如果轮播图的个数只有1张是不自动滚动的
 * 2、轮播个数大于1张的时候才会自动滚动
 */

#import <UIKit/UIKit.h>


@interface HMCircleScrollView : UIView

/// 轮播图模型数组 
@property (nonatomic, strong) NSArray *circleModelArr;

/// 自动滚动时间
@property (nonatomic, assign) NSTimeInterval autoScrollTimeInterval;

/// 实例化方法

+ (instancetype)circlrScrollWithFrame:(CGRect)frame placeholderImage:(UIImage *)placeholderImage;

@end
