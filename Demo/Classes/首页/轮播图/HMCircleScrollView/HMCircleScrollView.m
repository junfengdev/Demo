//
//  HMCircleScrollView.m
//  Demo
//
//  Created by lijunfeng on 2016/10/21.
//  Copyright © 2016年 hm. All rights reserved.
//
#define HMCircleScrollCellReuseIdentifier @"HMCircleScrollCellReuseIdentifier"
#define HMMaxSections 100

#import "HMCircleScrollView.h"
#import "HMCircleScrollCell.h"

@interface HMCircleScrollView ()<UICollectionViewDelegate,UICollectionViewDataSource>

/// 轮播图
@property (nonatomic, strong) UICollectionView *collectionView;
/// 轮播图布局
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
/// 滚动定时器
@property (nonatomic, weak) NSTimer *timer;

/// item的总个数
@property (nonatomic, assign) NSUInteger totalItemsCount;

@end

@implementation HMCircleScrollView{
    NSUInteger _currentItemIndex; // 当前itemIndex
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
        [self initialization];
    }
    return self;
}

- (void)initialization
{
    _totalItemsCount = 0;
    _autoScrollTimeInterval = 5;
}

+ (instancetype)circlrScrollWithFrame:(CGRect)frame placeholderImage:(UIImage *)placeholderImage{
    HMCircleScrollView *scroll = [[HMCircleScrollView alloc] initWithFrame:frame];
    return scroll;
}

- (void)setupUI{
    
    //flowLayout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 0;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _flowLayout = flowLayout;
    
    // collectionView
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    _collectionView = collectionView;
    collectionView.backgroundColor = [UIColor redColor];
    [self addSubview:collectionView];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.pagingEnabled = YES;
    collectionView.bounces = NO;
    collectionView.scrollsToTop = NO;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.showsVerticalScrollIndicator = NO;

    [_collectionView registerClass:[HMCircleScrollCell class] forCellWithReuseIdentifier:HMCircleScrollCellReuseIdentifier];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _totalItemsCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.circleModelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HMCircleScrollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:HMCircleScrollCellReuseIdentifier forIndexPath:indexPath];
    
    NSInteger next = indexPath.item;
    if (![self isBlankArr:self.circleModelArr]) {
        cell.imageView.image = [UIImage imageNamed:self.circleModelArr[next]];
        cell.label.text = [NSString stringWithFormat:@"第%ld页",(long)next];
    }
    return cell;
}


#pragma mark - UICollectionViewDelegate

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!self.circleModelArr.count) return;
    _currentItemIndex = [self currentIndex];
    NSLog(@"第%ld页",(unsigned long)_currentItemIndex);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self invalidateTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self setupTimer];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.flowLayout.itemSize = self.bounds.size;
}

- (void)setCircleModelArr:(NSArray *)circleModelArr{
    _circleModelArr = circleModelArr;
    
    if (![self isBlankArr:circleModelArr]) {
        _totalItemsCount = circleModelArr.count * 100; //默认可以无限循环
        [self.collectionView reloadData];

        if (circleModelArr.count > 1) { // 大于1张,开启定时器
            [self setupTimer];
            
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:HMMaxSections/2] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];

            self.collectionView.scrollEnabled = YES;
        }
        else{ // 只有一张
            self.collectionView.scrollEnabled = NO;
        }
        
    }
}

/// 创建定时器
- (void)setupTimer{
    NSTimeInterval timeInterval = self.autoScrollTimeInterval?self.autoScrollTimeInterval:5;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(automaticScroll) userInfo:nil repeats:YES];
    _timer = timer;
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)invalidateTimer{
    [_timer invalidate];
    _timer = nil;
}

- (void)automaticScroll{
    if (0 == _totalItemsCount) return;
    
    NSIndexPath *currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
    
    NSIndexPath *currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:HMMaxSections/2];
    [self.collectionView scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    
    NSInteger nextItem = currentIndexPathReset.item +1;
    NSInteger nextSection = currentIndexPathReset.section;
    if (nextItem==self.circleModelArr.count) {
        nextItem=0;
        nextSection++;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
}

- (NSUInteger)currentIndex
{
    if (self.collectionView.width == 0 || self.collectionView.height == 0) {
        return 0;
    }
    NSUInteger index = 0;
    index = (self.collectionView.contentOffset.x + _flowLayout.itemSize.width * 0.5) / _flowLayout.itemSize.width;
    return MAX(0, index);
}


- (BOOL)isBlankArr:(NSArray *)arr{
    if (arr == nil) {
        return YES;
    }
    if ([arr isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([arr isKindOfClass:[NSArray class]]){
        if (arr.count == 0) {
            return YES;
        }else{
            return NO;
        }
    }
    return NO;
}

@end
