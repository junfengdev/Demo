//
//  CircleScrollViewController.m
//  Demo
//
//  Created by lijunfeng on 2016/10/21.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "CircleScrollViewController.h"
#import "HMCircleScrollView.h"
@interface CircleScrollViewController ()

@end

@implementation CircleScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    HMCircleScrollView *scroll = [[HMCircleScrollView alloc] initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, SCREEN_WIDTH*0.62)];
    [self.view addSubview:scroll];
 
    scroll.circleModelArr = @[@"001.jpg",@"002.jpg",@"003.jpg",@"004.jpg"];

}

@end
