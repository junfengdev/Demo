//
//  UserTestModel.m
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "UserTestModel.h"

@implementation UserTestModel

+ (instancetype)shareInstance{
    static UserTestModel *model;
    static dispatch_once_t once_token;
    dispatch_once(&once_token, ^{
        model = [[self alloc] init];
    });
    return model;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.age forKey:@"age"];
    [aCoder encodeObject:self.uid forKey:@"uid"];
    [aCoder encodeObject:self.avatar forKey:@"avatar"];
    [aCoder encodeObject:self.phoneNum forKey:@"phoneNum"];
    [aCoder encodeObject:self.nickname forKey:@"nickname"];
    [aCoder encodeObject:self.introduce forKey:@"introduce"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
        self.age = [aDecoder decodeObjectForKey:@"age"];
        self.avatar = [aDecoder decodeObjectForKey:@"avatar"];
        self.nickname = [aDecoder decodeObjectForKey:@"nickname"];
        self.phoneNum = [aDecoder decodeObjectForKey:@"phoneNum"];
        self.introduce = [aDecoder decodeObjectForKey:@"introduce"];
    }
    return self;
}

- (void)save:(UserTestModel *)userModel{
    if (userModel) {
        NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:userModel];
        [[NSUserDefaults standardUserDefaults] setObject:userData forKey:kUserTestModel];
    }
}

- (UserTestModel *)getUser{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserTestModel]) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserTestModel];
        UserTestModel *userModel = (UserTestModel *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
        return userModel;
    }
    return nil;
}

+ (void)save:(UserTestModel *)userModel{
    [[UserTestModel shareInstance] save:userModel];
}

+ (UserTestModel *)getUser{
    return [[UserTestModel shareInstance]getUser];
}

/// 用户uid
+ (NSString *)uid{
    if ([[UserTestModel shareInstance] getUser]) {
        UserTestModel *user = [[UserTestModel shareInstance] getUser];
        return [NSString stringWithFormat:@"%@",user.uid];
    }
    return nil;
}

/// 用户昵称
+ (NSString *)nickname{
    if ([[UserTestModel shareInstance] getUser]) {
        UserTestModel *user = [[UserTestModel shareInstance] getUser];
        return [NSString stringWithFormat:@"%@",user.nickname];
    }
    return nil;
}

/// 用户头像
+ (NSString *)avatar{
    if ([[UserTestModel shareInstance] getUser]) {
        UserTestModel *user = [[UserTestModel shareInstance] getUser];
        return [NSString stringWithFormat:@"%@",user.avatar];
    }
    return nil;
}

/// 是否登录
+ (BOOL)isLogined{
    NSLog(@"uid:%@",[UserTestModel uid]);
    if ([[NSUserDefaults standardUserDefaults]objectForKey:kUserTestModel]) {
        if ([UserTestModel uid]) {
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }
}

@end
