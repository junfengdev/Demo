//
//  UserTestModel.h
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//
#define kUserTestModel @"k_User_TestModel"

#import <Foundation/Foundation.h>

@interface UserTestModel : NSObject<NSCoding>

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *age;
@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *introduce;

+ (instancetype)shareInstance;

+ (void)save:(UserTestModel *)userModel;
+ (UserTestModel *)getUser;

/// 用户uid
+ (NSString *)uid;

/// 用户昵称
+ (NSString *)nickname;

/// 用户头像
+ (NSString *)avatar;

/// 是否登录
+ (BOOL)isLogined;

@end
