//
//  UserDefualtViewController.m
//  Demo
//
//  Created by lijunfeng on 16/11/9.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "UserDefualtViewController.h"
#import "UserTestModel.h"

@interface UserDefualtViewController ()

@end

@implementation UserDefualtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UserTestModel *model = [UserTestModel shareInstance];
    model.age = @"18";
    model.phoneNum = @"qqqqqq";
    model.nickname = @"13615456844";
    model.avatar = @"www.huomao.com";
    model.introduce = @"爱神的箭法就是副科级奥斯卡";
    [UserTestModel save:model];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UserTestModel *user = [UserTestModel getUser];
    NSLog(@"%@,%@,%@,%@,%@",user.age,user.phoneNum,user.nickname,user.avatar,user.introduce);
    
    // 修改单个数据
    user.introduce = @"1111111111111111111111";
    [UserTestModel save:user];
    
    NSLog(@"%@,%@,%@",[UserTestModel uid],[UserTestModel nickname],[UserTestModel avatar]);

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserTestModel];
    
    if ([UserTestModel isLogined]) {
        NSLog(@"登录");
    }
    else{
        NSLog(@"未登录");
    }
}



@end
