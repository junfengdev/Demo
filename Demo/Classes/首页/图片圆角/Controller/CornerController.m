//
//  CornerController.m
//  Demo
//
//  Created by lijunfeng on 2016/10/16.
//  Copyright © 2016年 hm. All rights reserved.
//

#import "CornerController.h"
#import "UIImage+Category.h"

@interface CornerController ()

@end

@implementation CornerController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:scroll];
    
    CGFloat imgH = 100;
    
    int count = 100;
    
    for (int i = 0; i < count; i ++) {
        
        int X = i%3;
        
        NSTimeInterval start = CACurrentMediaTime();
        
        CGFloat imgY = 10+(100+20)*i;
        CGFloat imgX = 10+(imgH+20)*X;
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imgX, imgY, imgH, imgH)];
        [scroll addSubview:img];

        UIImage *image = [UIImage imageNamed:@"001.jpg"];

//        [image cornerImageWithSize:CGSizeMake(imgH, imgH) fillColor:[UIColor redColor] competion:^(UIImage *image) {
//            img.image = image;
//        }];
        
        img.image = image;
        img.layer.cornerRadius = imgH/2.0;
        img.clipsToBounds = YES;
        
        NSLog(@"%f",CACurrentMediaTime() - start);
    }
    
    [scroll setContentSize:CGSizeMake(SCREEN_WIDTH, 10+(20+imgH)*count)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
