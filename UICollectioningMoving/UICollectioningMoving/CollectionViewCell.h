//
//  CollectionViewCell.h
//  UICollectioningMoving
//
//  Created by lijunfeng on 2016/10/31.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIButton *btnDelete;
@property (nonatomic, strong) UILabel *lab;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, copy) NSString *imageName;
@end
