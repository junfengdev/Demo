//
//  ChannelManager.m
//  UICollectioningMoving
//
//  Created by lijunfeng on 16/11/1.
//  Copyright © 2016年 李俊峰. All rights reserved.
//
#define LibraryDirectoryChannelPath [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define MyChannelpath    @"/MyChannelpath.plist"

#import "ChannelManager.h"

@implementation ChannelManager

+ (void)insertMyChannelAry:(NSArray *)ary completionHander:(void(^)(BOOL success))handler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([ary isKindOfClass:[NSArray class]] && ![ary isKindOfClass:[NSNull class]]) {
            NSData * data = [NSKeyedArchiver archivedDataWithRootObject:ary];
            NSString * filePath = [LibraryDirectoryChannelPath stringByAppendingString:MyChannelpath];
            BOOL isSuccess = [data writeToFile:filePath atomically:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (handler != nil) {
                    handler(isSuccess);
                }
            });
        }
    });
}

+ (id)getMyChannelAry:(NSArray *)ary {
    NSString * filePath = [LibraryDirectoryChannelPath stringByAppendingString:MyChannelpath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        if (![[NSKeyedUnarchiver unarchiveObjectWithFile:filePath] isKindOfClass:[NSNull class]]) {
            if ([[NSKeyedUnarchiver unarchiveObjectWithFile:filePath] isKindOfClass:[NSArray class]]) {
                NSArray *arrBanner = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
                return arrBanner;
            }
        }
    }
    return nil;
}

@end
