//
//  CollectionSectionView.h
//  UICollectioningMoving
//
//  Created by lijunfeng on 16/11/1.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionSectionView : UICollectionReusableView

@property (nonatomic, copy) NSString *titleForSection;

@end
