//
//  UIImage+Category.h
//  Demo
//
//  Created by lijunfeng on 2016/10/16.
//  Copyright © 2016年 hm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)

/// 直接返回裁剪好的图片
- (UIImage *)cornerImageWithSize:(CGSize)size fillColor:(UIColor *)fillColor;

/// 以block方式异步回调
- (void)cornerImageWithSize:(CGSize)size fillColor:(UIColor *)fillColor competion:(void (^)(UIImage *image))competion;

@end
