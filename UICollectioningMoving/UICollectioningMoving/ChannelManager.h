//
//  ChannelManager.h
//  UICollectioningMoving
//
//  Created by lijunfeng on 16/11/1.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelManager : NSObject
+ (void)insertMyChannelAry:(NSArray *)ary completionHander:(void(^)(BOOL success))handler;
@end
