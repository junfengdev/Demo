//
//  CollectionSectionView.m
//  UICollectioningMoving
//
//  Created by lijunfeng on 16/11/1.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import "CollectionSectionView.h"

@implementation CollectionSectionView{
    UILabel *_lab;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _lab = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, frame.size.width-40, frame.size.height)];
        [self addSubview:_lab];
    }
    return self;
}

- (void)setTitleForSection:(NSString *)titleForSection{
    _lab.text = titleForSection;
}

@end
