//
//  CollectionViewCell.m
//  UICollectioningMoving
//
//  Created by lijunfeng on 2016/10/31.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import "CollectionViewCell.h"
#import "UIImage+Category.h"

@implementation CollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height-20)];
        [self.contentView addSubview:_imageView];
        
        _lab = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame), frame.size.width,20)];
        _lab.adjustsFontSizeToFitWidth = YES;
        _lab.textColor = [UIColor blackColor];
        [self.contentView addSubview:_lab];
        _lab.numberOfLines = 0;
        _lab.textAlignment = NSTextAlignmentCenter;
        
        _btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-20, 0, 20, 20)];
        _btnDelete.backgroundColor = [UIColor redColor];
        [_btnDelete setTitle:@"-" forState:UIControlStateNormal];
        [_btnDelete setHidden:YES];
        [self.contentView addSubview:_btnDelete];
    }
    
    return self;
}

- (void)setImageName:(NSString *)imageName{
    UIImage *imageN = [UIImage imageNamed:imageName];
    _imageView.image = imageN;
}


@end
