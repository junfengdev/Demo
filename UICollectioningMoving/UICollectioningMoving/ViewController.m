//
//  ViewController.m
//  UICollectioningMoving
//
//  Created by lijunfeng on 2016/10/31.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#define Screen_Height [UIScreen mainScreen].bounds.size.height
#define Screen_Width [UIScreen mainScreen].bounds.size.width

#import "ViewController.h"
#import "CollectionViewCell.h"
#import "CollectionSectionView.h"
#import "ChannelManager.h"

@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) CALayer *dotLayer;
@property (nonatomic, assign) CGFloat endPoint_x;
@property (nonatomic, assign) CGFloat endPoint_y;
@property (nonatomic, strong) UIBezierPath *path;

@property (nonatomic, strong) NSMutableArray *myChannelArr;
@property (nonatomic, strong) NSMutableArray *allChannelArr;

@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;

@end

@implementation ViewController

static UICollectionReusableView *reusableviewMyChannel = nil;
static UICollectionReusableView *reusableviewAllChannel = nil;

- (NSMutableArray *)myChannelArr{
    if (!_myChannelArr) {
        _myChannelArr = [[NSMutableArray alloc] init];
        
        for (int i = 1; i <= 6; i ++) {
            NSString * num = [NSString stringWithFormat:@"00%d",i];
            [_myChannelArr addObject:num];
        }
    }
    return _myChannelArr;
}

// 26
- (NSMutableArray *)allChannelArr{
    if (!_allChannelArr) {
        _allChannelArr = [[NSMutableArray alloc] init];
        for (int i = 7; i <= 60; i ++) {
            NSString *num = nil;
            if (i >= 7 && i <= 9) {
                num = [NSString stringWithFormat:@"00%d",i];
            }else{
                 num = [NSString stringWithFormat:@"0%d",i];
            }
            [_allChannelArr addObject:num];
        }
    }
    return _allChannelArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(onClickEdting)];
    [self.view addSubview:self.collectionView];
}

- (void)onClickEdting{
    self.editing =! self.editing;
    [self.collectionView reloadData];
    
    if (self.editing) {
        [self.navigationItem.rightBarButtonItem setTitle:@"完成"];
    }
    else{
        [self.navigationItem.rightBarButtonItem setTitle:@"编辑"];
    }
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout.minimumInteritemSpacing = 5;
        
        CGFloat width = ([UIScreen mainScreen].bounds.size.width-5*10)/4.0;
        _flowLayout.itemSize = CGSizeMake(width, width*1.68);
        _flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _flowLayout.headerReferenceSize = CGSizeMake(Screen_Width, 50);

        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 20, Screen_Width, Screen_Height - 20) collectionViewLayout:_flowLayout];
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.f];
        _collectionView.delegate = self;
        [_collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentiifer"];
        
        [_collectionView registerClass:[CollectionSectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionSectionView"];
        
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
        _longPress.minimumPressDuration = 0.25f;
        [_collectionView addGestureRecognizer:_longPress];
    }
    return _collectionView;
}

- (void)longPressAction:(UILongPressGestureRecognizer *)longGesture{
    [self lonePressAction_iOS9_before:longGesture];
}

static UIView *_screenshotView;          // 截图
static NSIndexPath *_currentIndexPath;   // 当前路径
static NSIndexPath *_lastIndexPath;      // 旧路径
- (void)lonePressAction_iOS9_before:(UILongPressGestureRecognizer *)longGesture{
    
    if(!self.editing) return;
    
    CGPoint point = [longGesture locationInView:longGesture.view];
    
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
    
    
    switch (longGesture.state) {
        case UIGestureRecognizerStateBegan:{
            
            if (indexPath.section != 0) {
                return;
            }
            //判断手势落点位置是否在Item上
            CGPoint location = [_longPress locationInView:self.collectionView];
            NSIndexPath* indexPath = [self.collectionView indexPathForItemAtPoint:location];
            if (!indexPath) return;
            
            _lastIndexPath = indexPath;
            UICollectionViewCell* targetCell = [self.collectionView cellForItemAtIndexPath:_lastIndexPath];
            //得到当前cell的映射(截图)
            UIView* cellView = [targetCell snapshotViewAfterScreenUpdates:YES];
            _screenshotView = cellView;
            _screenshotView.frame = cellView.frame;
            targetCell.hidden = YES;
            [self.collectionView addSubview:_screenshotView];
            _screenshotView.center = targetCell.center;

            [UIView animateWithDuration:0.25 animations:^{
                _screenshotView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            } completion:nil];
            
        }
            break;
        case UIGestureRecognizerStateChanged:{
            
            // 当前手指位置
            CGPoint currentPoint = [longGesture locationInView:self.collectionView];
            
            CGFloat width = ([UIScreen mainScreen].bounds.size.width-5*10)/4.0;
            
            UIWindow * window = [[[UIApplication sharedApplication]delegate] window];
            
            CGRect rect = [reusableviewAllChannel convertRect:reusableviewAllChannel.bounds toView:window];
            
            CGPoint Xpoint = currentPoint ;
            
            Xpoint.x = MIN(Screen_Width, point.x) ;
            
            Xpoint.x = MAX(0, point.x) ;
            
            Xpoint.y = MIN(( rect.origin.y - 30 - (width*1.68/2.0) + self.collectionView.contentOffset.y), point.y);
           
            // NSLog(@"point.y==%f,Xpoint.y==%f",point.y,Xpoint.y);
            
            // 移动
            _screenshotView.center = Xpoint;
            
            //更新cell的位置
            NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:point];
            if (indexPath == nil )  return;
            
            if ( indexPath.section == 0 ) {
                
                if (![indexPath isEqual:_lastIndexPath])
                {
                    //改变数据源
                    id obj = [self.myChannelArr objectAtIndex:_lastIndexPath.item];
                    [self.myChannelArr removeObjectAtIndex:_lastIndexPath.item];
                    [self.myChannelArr insertObject:obj atIndex:indexPath.item];
                    
                    [self.collectionView moveItemAtIndexPath:_lastIndexPath toIndexPath:indexPath];
                    _lastIndexPath = indexPath;
                }
            }
            
        }
            break;
            
        case UIGestureRecognizerStateEnded: {
            
            UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:_lastIndexPath];
            
            [UIView animateWithDuration:0.25 animations:^{
                _screenshotView.transform = CGAffineTransformIdentity;
                _screenshotView.center = cell.center;
            } completion:^(BOOL finished) {
                [_screenshotView removeFromSuperview];
                _screenshotView = nil;
                _lastIndexPath = nil;
                cell.hidden  = NO;
            }];
        }
            
            break ;
            
        case UIGestureRecognizerStateFailed:
        {
            NSLog(@"UIGestureRecognizerStateFailed") ;
        }
            break ;
            
            
        case UIGestureRecognizerStatePossible:
        {
            NSLog(@"UIGestureRecognizerStatePossible") ;
        }
            break ;
            
        case UIGestureRecognizerStateCancelled:
        {
            NSLog(@"UIGestureRecognizerStatePossible") ;
        }
            break ;
            
        default:{
            
            UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:_lastIndexPath];
            
            [UIView animateWithDuration:0.25 animations:^{
                _screenshotView.transform = CGAffineTransformIdentity;
                _screenshotView.center = cell.center;
            } completion:^(BOOL finished) {
                [_screenshotView removeFromSuperview];
                _screenshotView = nil;
                _lastIndexPath = nil;
                cell.hidden  = NO;
            }];
            
        }
            break;
    }
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)sourceIndexPath willMoveToIndexPath:(NSIndexPath *)destinationIndexPath
{
    id objc = [self.myChannelArr objectAtIndex:sourceIndexPath.item];
    [self.myChannelArr removeObject:objc];
    [self.myChannelArr insertObject:objc atIndex:destinationIndexPath.item];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return self.myChannelArr.count;
    }else{
        return self.allChannelArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentiifer" forIndexPath:indexPath];
        if (self.myChannelArr) {
            cell.lab.text = self.myChannelArr[indexPath.item];
            cell.imageName = [NSString stringWithFormat:@"%@.jpg",self.myChannelArr[indexPath.item]];
            cell.btnDelete.hidden = self.editing?NO:YES;
        }
      
        return cell;
    }
    else{
        CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentiifer" forIndexPath:indexPath];
        if (self.allChannelArr) {
            cell.lab.text = self.allChannelArr[indexPath.item];
            cell.imageName = [NSString stringWithFormat:@"%@.jpg",self.allChannelArr[indexPath.item]];
            cell.btnDelete.hidden = YES;
        }
       
        return cell;
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isEditing && indexPath.section == 0) {
        return YES;
    }
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(nonnull NSIndexPath *)sourceIndexPath toIndexPath:(nonnull NSIndexPath *)destinationIndexPath
{
    NSIndexPath *selectIndexPath = [self.collectionView indexPathForItemAtPoint:[_longPress locationInView:self.collectionView]];
    
    CollectionViewCell *cell = (CollectionViewCell *)[self.collectionView cellForItemAtIndexPath:selectIndexPath];
    [UIView animateWithDuration:0.28 animations:^{
        cell.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    }];
    
    [cell.btnDelete setHidden:YES];
    
    if (sourceIndexPath.section == 0 && destinationIndexPath.section == 0) {
        [self.myChannelArr exchangeObjectAtIndex:sourceIndexPath.item withObjectAtIndex:destinationIndexPath.item];
    }
    
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.isEditing) {
            if (self.myChannelArr.count > 1) {
                id model = [self.myChannelArr objectAtIndex:indexPath.item];
                [self.myChannelArr removeObject:model];
                [self.allChannelArr addObject:model];
                
                NSInteger index = 0;
                if (self.allChannelArr.count > 0) {
                    index = self.allChannelArr.count - 1;
                }
                
                CollectionViewCell *cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                cell.btnDelete.hidden = YES;
                [collectionView moveItemAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForItem:index inSection:1]];
            }
            else{
                NSLog(@"至少订阅1个栏目");
            }

        }
        else {
            // 选择某一个
            NSLog(@"选择的是：%@",self.myChannelArr[indexPath.item]);
        }
    }
    else {

        // 第二个区
        if (self.editing) {
            if (self.myChannelArr.count < 8) {
                id model = [self.allChannelArr objectAtIndex:indexPath.item];
                [self.myChannelArr addObject:model];
                [self.allChannelArr removeObject:model];
                
                NSInteger index = 0;
                if (self.myChannelArr.count > 0) {
                    index = self.myChannelArr.count - 1;
                }
                
                CollectionViewCell *cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                cell.btnDelete.hidden = NO;
                
                [collectionView moveItemAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            }
            else{
                NSLog(@"最多订阅8个栏目");
            }
            
        }
        else{
            NSLog(@"选择的是：%@",self.allChannelArr[indexPath.item]);
        }
    }
    
    [collectionView reloadData];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader)
    {
        CollectionSectionView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionSectionView" forIndexPath:indexPath];
        
        if (indexPath.section == 0) {
            reusableviewMyChannel = headerView;
            reusableviewMyChannel.backgroundColor = [UIColor clearColor];
            headerView.titleForSection = @"我的游戏";
            return reusableviewMyChannel;
        }
        else{
            reusableviewAllChannel = headerView;
            reusableviewAllChannel.backgroundColor = [UIColor clearColor];
            headerView.titleForSection = @"游戏推荐";
            return reusableviewAllChannel;
        }
    }
    else{
        return nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(Screen_Width, 40);
}

@end
