//
//  AppDelegate.h
//  UICollectioningMoving
//
//  Created by lijunfeng on 2016/10/31.
//  Copyright © 2016年 李俊峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

